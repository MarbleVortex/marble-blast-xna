﻿using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.Sim;
using GarageGames.Torque.T3D;
using GarageGames.Torque.TS;
using GarageGames.Torque.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Text;

namespace GarageGames.Torque.Examples.MarbleDemo
{
    public class Marble : TorqueObject
    {
        private static bool _initialized = false;

        protected MarbleInputComponent _marbleInput;
        protected MarbleCollisionComponent _collision;
        protected MarbleControlComponent _marbleControl;
        protected TSRenderComponent _render;
        protected MoveComponent _move;
        protected MarbleShadow _shadow;

        public MarbleCameraComponent MarbleCamera
        {
            get;
            private set;
        }

        public Vector3 SpawnPoint
        {
            get
            {
                return _marbleControl.SpawnPoint;
            }
            set
            {
                _marbleControl.SpawnPoint = value;
            }
        }

        public Marble(Vector3 position, string dtsPath)
        {
            Marble._setupParticles();
            MarbleControlComponent.OnMarbleSpawn = new OnSpawnDelegate(this.RespawnOnOOB);
            _render = new TSRenderComponent();
            _move = new MoveComponent();
            MarbleCamera = new MarbleCameraComponent();
            _marbleControl = new MarbleControlComponent();
            _collision = new MarbleCollisionComponent();
            _shadow = new MarbleShadow();
            _marbleInput = new MarbleInputComponent();
            _render.ShapeName = dtsPath;
            this.Marblize(_render.Shape, _render.ShapeInstance, 0.2f);
            _render.Scale = new Vector3(1.5f, 1.5f, 1.5f);
            _marbleControl.Radius = 0.3f;
            _marbleControl.SpawnPoint = position;
            _collision.Radius = 0.3f;
            _move.ExportRenderTransform = true;
            _move.Gravity = new Vector3(0f, 0f, 0f);
            _move.Elasticity = 0f;
            _move.Friction = 0.8f;
            _move.Transform = _marbleControl.SpawnMatrix;
            MarbleCamera.Radius = 0.3f;
            //input.LookDirect = true;
            //input.AllowMouse = false;
            //input.ClearTrigger = false;

            this.Components.AddComponent(_render);
            this.Components.AddComponent(_move);
            this.Components.AddComponent(MarbleCamera);
            this.Components.AddComponent(_marbleInput);
            this.Components.AddComponent(_marbleControl);
            this.Components.AddComponent(_collision);
            this.Components.AddComponent(_shadow);

            TorqueObjectDatabase.Instance.Register(this);
            TorqueObjectDatabase.Instance.SetObjectType(this, "marble", true);
        }

        private static void _setupParticles()
        {
            if (Marble._initialized)
                return;

            MarbleControlComponent.WaveBank = new WaveBank(MarbleBlast.Instance.Engine.SFXDevice, "data\\sounds\\marble.xwb");
            MarbleControlComponent.SoundBank = new SoundBank(MarbleBlast.Instance.Engine.SFXDevice, "data\\sounds\\marble.xsb");

            if (ParticleEmitter.TurnOn)
            {
                ParticleData particleData = new ParticleData();
                particleData.MaterialName = "data\\shapes\\marbles\\burst.png";
                particleData.DragCoefficient = 0.5f;
                particleData.GravityCoefficient = -0.1f;
                particleData.ConstantAccleration = -2f;
                particleData.LifeTimeMS = 400;
                particleData.LifeTimeVarianceMS = 50;
                particleData.UseInvAlpha = false;
                particleData.SpinSpeed = 90f;
                particleData.SpinRandomMin = -90f;
                particleData.SpinRandomMax = 90f;
                particleData.Colors[0] = new Vector4(0.5f, 0.5f, 0.5f, 0.3f);
                particleData.Colors[1] = new Vector4(0.3f, 0.3f, 0.2f, 0.1f);
                particleData.Colors[2] = new Vector4(0.2f, 0.2f, 0.1f, 0f);
                particleData.Sizes[0] = 0.8f;
                particleData.Sizes[1] = 0.4f;
                particleData.Sizes[2] = 0.2f;
                particleData.Times[0] = 0f;
                particleData.Times[1] = 0.75f;
                particleData.Times[2] = 1f;
                MarbleControlComponent.BounceEmitterData = new ParticleEmitterData
                {
                    ParticleData = particleData,
                    EjectionPeriodMS = 10,
                    PeriodVarianceMS = 0,
                    EjectionVelocity = 6f,
                    VelocityVariance = 0.25f,
                    ThetaMin = 80f,
                    ThetaMax = 90f,
                    LifetimeMS = 250
                };
            }
            Marble._initialized = true;
        }

        protected void RespawnOnOOB()
        {
            if (this.MarbleCamera.IsOOB)
            {
                this.MarbleCamera.IsOOB = false;
                InputManager.Instance.PopInputMap(MarbleControlComponent.OOBInputMap);
                TorqueObject obj = this.MarbleCamera.Owner;
                if (obj == null)
                {
                    return;
                }
                MoveComponent move = obj.Components.FindComponent<MoveComponent>();
                if (move == null)
                {
                    return;
                }
                MarbleControlComponent marble = obj.Components.FindComponent<MarbleControlComponent>();
                if (marble == null)
                {
                    return;
                }
                move.Transform = marble.SpawnMatrix;
                move.Velocity = default(Vector3);
                move.RotVelocity = default(Vector3);
            }
        }

        protected virtual void RespawnOnOOB(MarbleControlComponent marble, MoveComponent move)
        {
            int spawnIdx = TorqueUtil.GetRandomInt(MarbleBlast.Instance.LevelManager.SpawnGroup.Count);
            marble.SpawnPoint = MarbleBlast.Instance.LevelManager.SpawnGroup[spawnIdx];

            move.Transform = marble.SpawnMatrix;
            move.Velocity = default(Vector3);
            move.RotVelocity = default(Vector3);
        }

        protected void Marblize(Shape shape, ShapeInstance shapeInstance, float radius)
        {
            for (int i = 0; i < shape.nodes.Length; i++)
            {
                Node node = shape.nodes[i];
                shape.defaultTranslations[i] = default(Vector3);
                shapeInstance.NodeTransforms[i].Translation = default(Vector3);
                if (node._firstObject >= 0)
                {
                    GarageGames.Torque.TS.Object obj = shape.objects[node._firstObject];
                    if (obj._startMeshIndex >= 0)
                    {
                        Mesh mesh = shape.meshes[obj._startMeshIndex];
                        for (int j = 0; j < mesh._verts.Length; j++)
                        {
                            if (mesh._verts[j].LengthSquared() > 1E-07f)
                            {
                                mesh._verts[j].Normalize();
                                mesh._verts[j] *= radius;
                            }
                        }
                    }
                }
            }
        }

        public void SetControl(bool inControl)
        {
            this._marbleInput.SetControl(inControl);
        }
    }
}
