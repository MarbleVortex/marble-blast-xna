﻿using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.Platform;
using GarageGames.Torque.Sim;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace GarageGames.Torque.Examples.MarbleDemo
{
    public class MarbleInputComponent : TorqueComponent, IAnimatedObject
    {
        private InputInterface _input;
        private InputMap _inputMap;
        private MarbleCameraComponent _cameraComponent;

        private MarbleCameraComponent CameraComponent
        {
            get
            {
                if (_cameraComponent == null)
                    _cameraComponent = this.Owner.Components.FindComponent<MarbleCameraComponent>();
                return _cameraComponent;
            }
            set
            {
                _cameraComponent = value;
            }
        }

        private bool _leftTrigger, _rightTrigger;
        private bool _leftBumper, _rightBumper;

        private float _x, _xPlus, _xMinus;
        private float _y, _yPlus, _yMinus;

        private bool _lookDirect;
        private bool _clearMouse;

        private bool _active;

        private Vector2 _deltaMouse;
        private Vector2 _lastMouse;

        public MarbleInputComponent()
        {
            this._input = new InputInterface();
            this._lookDirect = true;
        }

        private void InputOnA(float val)
        {
            this._leftTrigger = (val > 0.5f);
        }

        // Movement
        private void InputMoveX(float val)
        {
            this._x = val;
        }

        private void InputMoveY(float val)
        {
            this._y = val;
        }

        private void InputMoveXPlus(float val)
        {
            this._xPlus = val;
        }

        private void InputMoveXMinus(float val)
        {
            this._xMinus = -val;
        }

        private void InputMoveYPlus(float val)
        {
            this._yPlus = val;
        }

        private void InputMoveYMinus(float val)
        {
            this._yMinus = -val;
        }

        // Camera
        private void InputLookX(float val)
        {
            this._deltaMouse.X = val;
            this._clearMouse = false;
            this.CameraComponent.InstantCamera = false;
        }

        private void InputLookY(float val)
        {
            this._deltaMouse.Y = val;
            this._clearMouse = false;
            this.CameraComponent.InstantCamera = false;
        }

        private void InputLookXPlus(float val)
        {
            this._deltaMouse.X = val;
            this._clearMouse = false;
            this.CameraComponent.InstantCamera = false;
        }

        private void InputLookXMinus(float val)
        {
            this._deltaMouse.X = -val;
            this._clearMouse = false;
            this.CameraComponent.InstantCamera = false;
        }

        private void InputLookYPlus(float val)
        {
            this._deltaMouse.Y = val;
            this._clearMouse = false;
            this.CameraComponent.InstantCamera = false;
        }

        private void InputLookYMinus(float val)
        {
            this._deltaMouse.Y = -val;
            this._clearMouse = false;
            this.CameraComponent.InstantCamera = false;
        }

        private void InputMouseLookX(float val)
        {
            this._deltaMouse.X -= val;
            this._clearMouse = true;
            this.CameraComponent.InstantCamera = true;
        }

        private void InputMouseLookY(float val)
        {
            this._deltaMouse.Y += val;
            this._clearMouse = true;
            this.CameraComponent.InstantCamera = true;
        }

        private void _applyMapping()
        {
            if (this._inputMap != null)
                return;

            this._inputMap = new InputMap();
            int gamepadId = InputManager.Instance.FindDevice("gamepad0");
            int keyboardId = InputManager.Instance.FindDevice("keyboard");
            int mouseId = InputManager.Instance.FindDevice("mouse");

            // Gamepad
            this._inputMap.BindAction(gamepadId, (int)XGamePadDevice.GamePadObjects.LeftThumbX, new InputMap.ActionDelegate(this.InputMoveX));
            this._inputMap.SetBindDeadZone(-0.25f, 0.25f);
            this._inputMap.BindAction(gamepadId, (int)XGamePadDevice.GamePadObjects.LeftThumbY, new InputMap.ActionDelegate(this.InputMoveY));
            this._inputMap.SetBindDeadZone(-0.25f, 0.25f);
            this._inputMap.BindAction(gamepadId, (int)XGamePadDevice.GamePadObjects.RightThumbX, new InputMap.ActionDelegate(this.InputLookX));
            this._inputMap.BindAction(gamepadId, (int)XGamePadDevice.GamePadObjects.RightThumbY, new InputMap.ActionDelegate(this.InputLookY));
            this._inputMap.BindAction(gamepadId, (int)XGamePadDevice.GamePadObjects.A, new InputMap.ActionDelegate(this.InputOnA));
            //this._inputMap.BindAction(gamepadId, 4096, new InputMap.ActionDelegate(this.InputOnX));
            this._inputMap.BindAction(gamepadId, (int)XGamePadDevice.GamePadObjects.LeftTrigger, new InputMap.ActionDelegate(this.InputOnA));//this.InputOnLT));
            //this._inputMap.BindAction(gamepadId, 134217728, new InputMap.ActionDelegate(this.InputOnRT));
            //this._inputMap.BindCommand(gamepadId, 8192, new InputMap.MakeDelegate(this.InputOnY), null);

            // Keyboard
            this._inputMap.BindAction(keyboardId, (int)Keys.D, new InputMap.ActionDelegate(this.InputMoveXPlus));
            this._inputMap.BindAction(keyboardId, (int)Keys.A, new InputMap.ActionDelegate(this.InputMoveXMinus));
            this._inputMap.BindAction(keyboardId, (int)Keys.W, new InputMap.ActionDelegate(this.InputMoveYPlus));
            this._inputMap.BindAction(keyboardId, (int)Keys.S, new InputMap.ActionDelegate(this.InputMoveYMinus));
            this._inputMap.BindAction(keyboardId, (int)Keys.Right, new InputMap.ActionDelegate(this.InputLookXPlus));
            this._inputMap.BindAction(keyboardId, (int)Keys.Left, new InputMap.ActionDelegate(this.InputLookXMinus));
            this._inputMap.BindAction(keyboardId, (int)Keys.Up, new InputMap.ActionDelegate(this.InputLookYPlus));
            this._inputMap.BindAction(keyboardId, (int)Keys.Down, new InputMap.ActionDelegate(this.InputLookYMinus));
            this._inputMap.BindAction(keyboardId, (int)Keys.Space, new InputMap.ActionDelegate(this.InputOnA));

            // Mouse
            //this._inputMap.BindAction(mouseId, (int)XMouseDevice.Action.Move, new InputMap.ActionDelegate(this.InputMouseLookX));
            //this._inputMap.BindAction(mouseId, (int)XMouseDevice.Action.Shift, new InputMap.ActionDelegate(this.InputMouseLookY));
        }

        public void SetControl(bool inControl)
        {
            this._active = inControl;

            this._applyMapping();

            if (inControl)
            {
                InputManager.Instance.PushInputMap(this._inputMap);
                ProcessList.Instance.AddAnimationCallback(this.Owner, this);
                ProcessList.Instance.SetEnabled(this.Owner, true);
            } else
            {
                InputManager.Instance.PopInputMap(this._inputMap);
                ProcessList.Instance.SetEnabled(this.Owner, false);
            }
        }

        public void UpdateInput(float dt)
        {
            float centerX = GraphicsDeviceManager.DefaultBackBufferWidth / 2.0f;
            float centerY = GraphicsDeviceManager.DefaultBackBufferHeight / 2.0f;

            Vector2 look = this._input.Look;
            Vector2 move = this._input.Move;

            // X Movement
            if (Math.Abs(this._x) > Math.Abs(this._xPlus + this._xMinus))
            {
                move.X = this._x;
            } else
            {
                move.X = this._xPlus + this._xMinus;
            }

            // Y Movement
            if (Math.Abs(this._y) > Math.Abs(this._yPlus + this._yMinus))
            {
                move.Y = this._y;
            } else
            {
                move.Y = this._yPlus + this._yMinus;
            }

            //int dx = (int)(Mouse.GetState().X - this._lastMouse.X);
            //int dy = (int)(Mouse.GetState().Y - this._lastMouse.Y);

            //this._deltaMouse.X = Mouse.GetState().X;
            //this._deltaMouse.Y = Mouse.GetState().Y;

            /*if (dx != 0 || dy != 0)
            {
                look.X = dx;
                look.Y = -dy;
            } else */

            //look.X = dx;
            //look.Y = dy;
            
            //Vector2 currentMouse = this._deltaMouse - this._lastMouse;



            if (this._lookDirect)
            {
                look = _deltaMouse;
            } else
            {
                // Look
                look.X += 1.5f * this._deltaMouse.X * (float)Math.PI / 180f;
                look.Y += this._deltaMouse.Y * 0.25f;
            }

            if (_clearMouse)
            {
                this._deltaMouse.X = 0f;
                this._deltaMouse.Y = 0f;
            }

            float signY = (look.Y > 0f ? 1f : -1f);
            if (look.Y * signY > 1f)
            {
                look.Y = signY;
            }

            // Triggers
            this._input.LeftTrigger = this._leftTrigger;
            this._input.RightTrigger = this._rightTrigger;

            this._input.Look = look;
            this._input.Move = move;
            //Mouse.SetPosition((int)centerX, (int)centerY);

            //this._lastMouse = this._deltaMouse;

            //this._lastMouse.X = Mouse.GetState().X;
            //this._lastMouse.Y = Mouse.GetState().Y;
        }

        public void UpdateAnimation(float dt)
        {
            this.UpdateInput(dt);
        }

        protected override bool _OnRegister(TorqueObject owner)
        {
            base._OnRegister(owner);
            return true;
        }

        protected override void _OnUnregister()
        {
            base._OnUnregister();
            this.SetControl(false);
        }

        protected override void _RegisterInterfaces(TorqueObject owner)
        {
            owner.RegisterCachedInterface("input", "", this, this._input);
            base._RegisterInterfaces(owner);
        }
    }
}
