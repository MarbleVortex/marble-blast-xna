using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.T3D;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public class MarbleCollisionComponent : TorqueComponent
	{
		public class MarbleCollisionInterface : CollisionInterface
		{
			public override float update(ref Vector3 pos1, Vector3 pos0, ref Quaternion rot1, Quaternion rot0, float dt, List<CollisionInfo> contacts)
			{
				Assert.Fatal(base.Owner is MarbleCollisionComponent, "Wrong component type");
				return (base.Owner as MarbleCollisionComponent).Update(ref pos1, pos0, ref rot1, rot0, dt, contacts);
			}

			public override void FindContacts(Vector3 pos, Quaternion rot, List<CollisionInfo> contacts)
			{
				Assert.Fatal(base.Owner is MarbleCollisionComponent, "Wrong component type");
				(base.Owner as MarbleCollisionComponent).FindContacts(pos, rot, contacts);
			}
		}

		private MarbleCollisionComponent.MarbleCollisionInterface _collisionInterface;

		private CollisionResponseInterface _collisionResponseInterface;

		private ValueInterface<Vector3> _velocityInterface;

		private ValueInterface<Matrix> _worldTransformInterface;

		private ValueInterface<Box3F> _objectBoxInterface;

		private TorqueObjectType _marbleType;

		private float _radius = 0.2f;

		private bool _ignoreMarbles;

		private static T3DSceneContainerQueryData _query = new T3DSceneContainerQueryData();

		private static List<ISceneContainerObject> _queryResult = new List<ISceneContainerObject>();

		public float Radius
		{
			get
			{
				return this._radius;
			}
			set
			{
				this._radius = value;
			}
		}

		public bool IgnoreMarbles
		{
			get
			{
				return this._ignoreMarbles;
			}
			set
			{
				this._ignoreMarbles = value;
			}
		}

		public MarbleCollisionComponent()
		{
			this._collisionInterface = new MarbleCollisionComponent.MarbleCollisionInterface();
		}

		public float Update(ref Vector3 pos1, Vector3 pos0, ref Quaternion rot1, Quaternion rot0, float dt, List<CollisionInfo> contacts)
		{
			float dt2 = dt;
			Box3F searchBox = this._objectBoxInterface.Value;
			Box3F startBox = searchBox;
			Vector3 rad = new Vector3(this._radius, this._radius, this._radius);
			startBox.Min += pos0 - rad;
			startBox.Max += pos0 + rad;
			Box3F endBox = searchBox;
			endBox.Min += pos1 - rad;
			endBox.Max += pos1 + rad;
			searchBox = startBox;
			searchBox.Extend(endBox.Min);
			searchBox.Extend(endBox.Max);
			MarbleCollisionComponent._query.WorldBox = searchBox;
			MarbleCollisionComponent._query.ResultList = MarbleCollisionComponent._queryResult;
			MarbleCollisionComponent._query.ResultList.Clear();
			T3DSceneGraph.Instance.Container.FindObjects(MarbleCollisionComponent._query);
			foreach (ISceneObject so in MarbleCollisionComponent._queryResult)
			{
				StaticGeometryComponent stat = so as StaticGeometryComponent;
				TorqueComponent marble = so as TorqueComponent;
				if (stat != null)
				{
					dt2 = Math.Min(dt2, dt * stat.FindIntersectionTime(pos0, pos1, this._radius));
				}
				else if (!this._ignoreMarbles && marble != null && marble.Owner.TestObjectType(this._marbleType))
				{
					MarbleCollisionComponent collision = marble.Owner.Components.FindComponent<MarbleCollisionComponent>();
					if (collision != null && collision != this)
					{
						dt2 = Math.Min(dt2, dt * collision._FindMarbleCollisionTime(pos0, pos1, dt, this._radius));
					}
				}
			}
			float i;
			if (dt - dt2 < 0.001f)
			{
				i = dt2 / dt;
				pos1 = i * (pos1 - pos0) + pos0;
				return 0f;
			}
			i = dt2 / dt;
			pos1 = i * (pos1 - pos0) + pos0;
			return dt - dt2;
		}

		public void FindContacts(Vector3 pos, Quaternion rot, List<CollisionInfo> contacts)
		{
			float ContactThreshold = 0.0001f;
			Box3F searchBox = this._objectBoxInterface.Value;
			Vector3 rad = new Vector3(this._radius, this._radius, this._radius);
			searchBox.Min += pos - rad;
			searchBox.Max += pos + rad;
			MarbleCollisionComponent._query.WorldBox = searchBox;
			MarbleCollisionComponent._query.ResultList = MarbleCollisionComponent._queryResult;
			MarbleCollisionComponent._query.ResultList.Clear();
			T3DSceneGraph.Instance.Container.FindObjects(MarbleCollisionComponent._query);
			foreach (ISceneObject so in MarbleCollisionComponent._queryResult)
			{
				StaticGeometryComponent stat = so as StaticGeometryComponent;
				TorqueComponent marble = so as TorqueComponent;
				if (stat != null)
				{
					stat.AddContact(pos, this._radius + ContactThreshold, contacts);
				}
				else if (!this._ignoreMarbles && marble != null && marble.Owner.TestObjectType(this._marbleType))
				{
					TorqueObject obj = marble.Owner;
					int i = 0;
					while (i < obj.Components.GetNumComponents())
					{
						MarbleCollisionComponent collision = obj.Components.GetComponentByIndex(i) as MarbleCollisionComponent;
						if (collision != null)
						{
							if (collision != this)
							{
								collision._AddContact(pos, this._radius + ContactThreshold, contacts);
								break;
							}
							break;
						}
						else
						{
							i++;
						}
					}
				}
			}
		}

		public override void CopyTo(TorqueComponent obj)
		{
			base.CopyTo(obj);
			MarbleCollisionComponent obj2 = (MarbleCollisionComponent)obj;
			obj2.Radius = this.Radius;
			obj2.IgnoreMarbles = this.IgnoreMarbles;
		}

		protected override bool _OnRegister(TorqueObject owner)
		{
			if (!base._OnRegister(owner))
			{
				return false;
			}
			this._velocityInterface = (base.Owner.Components.GetInterface("vector3", "velocity") as ValueInterface<Vector3>);
			this._worldTransformInterface = (base.Owner.Components.GetInterface("transform", "render") as ValueInterface<Matrix>);
			this._collisionResponseInterface = (base.Owner.Components.GetInterface("collisionResponse", string.Empty) as CollisionResponseInterface);
			this._objectBoxInterface = (base.Owner.Components.GetInterface("bounds", "object") as ValueInterface<Box3F>);
			this._marbleType = base.Owner.Manager.GetObjectType("marble");
			return true;
		}

		protected override void _RegisterInterfaces(TorqueObject owner)
		{
			owner.RegisterCachedInterface("collision", string.Empty, this, this._collisionInterface);
			base._RegisterInterfaces(owner);
		}

		protected float _FindMarbleCollisionTime(Vector3 pos0, Vector3 pos1, float dt, float themRadius)
		{
			float t = 1f;
			Vector3 usCenter = this._worldTransformInterface.Value.Translation;
			float tCap;
			if (!Collision.IntersectSegmentCapsule(pos0, pos1, usCenter, usCenter, themRadius + this._radius, out t, out tCap))
			{
				return 1f;
			}
			Vector3 delta = pos1 - pos0;
			Vector3 endPos = pos0 + t * delta;
			Vector3 norm = endPos - usCenter;
			norm.Normalize();
			Vector3 deltaVel = pos1 - pos0 - dt * this._velocityInterface.Value;
			if (Vector3.Dot(deltaVel, norm) >= 0f)
			{
				return 1f;
			}
			return t;
		}

		protected void _AddContact(Vector3 pos, float radius, List<CollisionInfo> contacts)
		{
			Vector3 usCenter = this._worldTransformInterface.Value.Translation;
			float distSq = (usCenter - pos).LengthSquared();
			if (distSq <= (radius + this._radius) * (radius + this._radius))
			{
				Vector3 normal = pos - usCenter;
				normal.Normalize();
				Vector3 collPos = usCenter + normal * this._radius;
				CollisionInfo info = default(CollisionInfo);
				info.collider = this._collisionResponseInterface;
				info.velocity = this._velocityInterface.Value;
				info.normal = normal;
				info.point = collPos;
				info.penetration = radius - Vector3.Dot(pos - info.point, normal);
				info.restitution = 0.5f;
				info.friction = 0f;
				contacts.Add(info);
			}
		}
	}
}
