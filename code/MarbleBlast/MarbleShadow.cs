using GarageGames.Torque.Core;
using GarageGames.Torque.GFX;
using GarageGames.Torque.LightingSystem;
using GarageGames.Torque.Materials;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.RenderManager;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.T3D;
using GarageGames.Torque.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public class MarbleShadow : TorqueComponent, ISceneObject3D, ISceneContainerObject, ISceneObject, ILightSetup
	{
		private float _ShadowRadius = 6f;

		private int _MaxVerts = 500;

		private float _Radius = 0.5f;

		private float _ShadowLength = 5f;

		private ValueInterface<Matrix> _worldTransformInterface;

		private SceneContainerData _sceneContainerData = new SceneContainerData();

		private ValueInPlaceInterface<ISceneObject> _renderInterface;

		private Resource<VertexBuffer> _vb;

		private static T3DSceneContainerQueryData _query = new T3DSceneContainerQueryData();

		private static List<ISceneContainerObject> _queryResult = new List<ISceneContainerObject>();

		private static RenderMaterial _material;

		public Matrix Transform
		{
			get
			{
				return this._worldTransformInterface.Value;
			}
		}

		public bool Visible
		{
			get
			{
				return true;
			}
		}

		public float VisibilityLevel
		{
			get
			{
				return 1f;
			}
		}

		public TorqueObjectType ObjectType
		{
			get
			{
				return TorqueObjectDatabase.Instance.GenericObjectType;
			}
		}

		public Box3F WorldBox
		{
			get
			{
				Vector3 pos = this._worldTransformInterface.Value.Translation;
				Box3F outBox = default(Box3F);
				outBox.Min = (outBox.Max = pos);
				Vector3 rad = new Vector3(this._ShadowRadius, this._ShadowRadius, this._ShadowRadius);
				outBox.Min -= rad;
				outBox.Max += rad;
				return outBox;
			}
		}

		[TorqueCloneIgnore]
		public SceneContainerData SceneContainerData
		{
			get
			{
				return this._sceneContainerData;
			}
			set
			{
				this._sceneContainerData = value;
			}
		}

		public OnCollisionDelegate OnCollision
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		public MarbleShadow()
		{
			this._renderInterface = new ValueInPlaceInterface<ISceneObject>();
		}

		public void Render(SceneRenderState srs)
		{
			if (MarbleShadow._material == null)
			{
				MarbleShadow._material = RenderMaterialManager.Lookup("data\\shapes\\marbles\\shadow.png");
			}
			if (MarbleShadow._material == null)
			{
				return;
			}
			this.SetupLights();
			Vector3 lightDir = new Vector3(0f, 0f, -1f);
			DirectionalLightInfo light = BaseSceneGraph.Instance.GetLightManager().GetSunLight() as DirectionalLightInfo;
			if (light != null)
			{
				lightDir = light.Direction;
			}
			Vector3 pos = this._worldTransformInterface.Value.Translation;
			Box3F outBox = default(Box3F);
			outBox.Min = (outBox.Max = pos);
			Vector3 rad = new Vector3(this._Radius, this._Radius, this._Radius);
			outBox.Min -= rad;
			outBox.Max += rad;
			if (lightDir.X > 0f)
			{
				outBox.Max.X = outBox.Max.X + lightDir.X * this._ShadowLength;
			}
			else
			{
				outBox.Min.X = outBox.Min.X + lightDir.X * this._ShadowLength;
			}
			if (lightDir.Y > 0f)
			{
				outBox.Max.Y = outBox.Max.Y + lightDir.Y * this._ShadowLength;
			}
			else
			{
				outBox.Min.Y = outBox.Min.Y + lightDir.Y * this._ShadowLength;
			}
			if (lightDir.Z > 0f)
			{
				outBox.Max.Z = outBox.Max.Z + lightDir.Z * this._ShadowLength;
			}
			else
			{
				outBox.Min.Z = outBox.Min.Z + lightDir.Z * this._ShadowLength;
			}
			List<StaticGeometryComponent.Triangle> triList;
			List<Vector3> pointList;
			List<Vector3> normList;
			this._FindPolys(outBox, out triList, out pointList, out normList);
			if (triList != null)
			{
				if (this._vb.IsNull)
				{
					this._CreateVB();
				}
				int numTris = this._FillVB(triList, pointList, normList, lightDir, pos);
				if (numTris > 0)
				{
					RenderInstance ri = SceneRenderer.RenderManager.AllocateInstance();
					ri.SceneObject = srs.CurrentSceneObject;
					ri.VertexSize = GFXVertexFormat.VertexSize;
					ri.VertexDeclaration = GFXVertexFormat.GetVertexDeclaration(srs.Gfx.Device);
					ri.VertexBuffer = this._vb.Instance;
					ri.IndexBuffer = null;
					ri.NumVerts = numTris * 3;
					ri.BaseVertex = 0;
					ri.PrimitiveType = PrimitiveType.TriangleList;
					ri.PrimitiveCount = numTris;
					ri.Material = MarbleShadow._material;
					ri.StartIndex = 0;
					ri.Type = RenderInstance.RenderInstanceType.Shadow;
					ri.TexU = TextureAddressMode.Clamp;
					ri.TexV = TextureAddressMode.Clamp;
					ri.ProjectionTransform = srs.Projection;
					ri.ObjectTransform = srs.World.Top;
					ri.RotationOnly = Matrix.Identity;
					BaseSceneGraph.Instance.GetLightManager().GetBestLights(ri.Lights);
					SceneRenderer.RenderManager.AddInstance(ri);
				}
			}
			this.ResetLights();
		}

		public void SetupLights()
		{
			BaseSceneGraph.Instance.GetLightManager().SetupLights(null, this.WorldBox);
		}

		public void ResetLights()
		{
			BaseSceneGraph.Instance.GetLightManager().ResetLights();
		}

		protected override bool _OnRegister(TorqueObject owner)
		{
			base._OnRegister(owner);
			this._worldTransformInterface = (base.Owner.Components.GetInterface("transform", "render") as ValueInterface<Matrix>);
			return this._worldTransformInterface != null;
		}

		protected override void _RegisterInterfaces(TorqueObject owner)
		{
			this._renderInterface.Value = this;
			owner.RegisterCachedInterface("render", string.Empty, this, this._renderInterface);
			base._RegisterInterfaces(owner);
		}

		private void _CreateVB()
		{
			if (!this._vb.IsNull)
			{
				return;
			}
			Assert.Fatal(this._vb.IsNull, "doh");
			int sizeInBytes = this._MaxVerts * GFXVertexFormat.VertexSize;
			this._vb = ResourceManager.Instance.CreateVertexBuffer(ResourceProfiles.ManualStaticVBProfile, sizeInBytes);
		}

		private int _FillVB(List<StaticGeometryComponent.Triangle> triList, List<Vector3> pointList, List<Vector3> normList, Vector3 lightDir, Vector3 casterPos)
		{
			float invRadius = 1f / this._Radius;
			float invShadowLength = 1f / this._ShadowLength;
			Assert.Fatal(!this._vb.IsNull, "doh");
			Assert.Fatal(Math.Abs(lightDir.Z) > 0.3f, "light direction must be downward for shadows to work");
			Vector3 x = new Vector3(1f, 0f, 0f);
			Vector3 y = Vector3.Cross(x, lightDir);
			y.Normalize();
			x = Vector3.Cross(lightDir, y);
			Vector3 farPoint = casterPos + this._ShadowLength * lightDir;
			float xDot = Vector3.Dot(x, casterPos);
			float yDot = Vector3.Dot(y, casterPos);
			float lDot = Vector3.Dot(lightDir, casterPos);
			int vidx = 0;
			int numVerts = Math.Max(triList.Count * 3, this._MaxVerts);
			int arg_D0_0 = GFXVertexFormat.VertexSize;
			GFXVertexFormat.PCTTBN[] vertices = TorqueUtil.GetScratchArray<GFXVertexFormat.PCTTBN>(numVerts);
			for (int i = 0; i < triList.Count; i++)
			{
				Vector3 v0 = pointList[triList[i].idx0];
				Vector3 v = pointList[triList[i].idx1];
				Vector3 v2 = pointList[triList[i].idx2];
				float t;
				Vector3 closest;
				if (Collision.IntersectMovingSphereTriangle(casterPos, farPoint, this._Radius, v0, v, v2, normList[triList[i].normalIdx], out t, out closest))
				{
					float tx = 0.5f * (1f + (Vector3.Dot(v0, x) - xDot) * invRadius);
					float ty = 0.5f * (1f + (Vector3.Dot(v0, y) - yDot) * invRadius);
					float alpha = 1f - (Vector3.Dot(v0, lightDir) - lDot) * invShadowLength;
					vertices[vidx++] = new GFXVertexFormat.PCTTBN(new Vector4(v0.X, v0.Y, v0.Z, 1f), new Vector4(1f, 1f, 1f, alpha), new Vector2(tx, ty), new Vector4(1f, 0f, 0f, 0f), new Vector4(0f, 1f, 0f, 0f), new Vector4(0f, 0f, 1f, 0f));
					tx = 0.5f * (1f + (Vector3.Dot(v, x) - xDot) * invRadius);
					ty = 0.5f * (1f + (Vector3.Dot(v, y) - yDot) * invRadius);
					alpha = 1f - (Vector3.Dot(v, lightDir) - lDot) * invShadowLength;
					vertices[vidx++] = new GFXVertexFormat.PCTTBN(new Vector4(v.X, v.Y, v.Z, 1f), new Vector4(1f, 1f, 1f, alpha), new Vector2(tx, ty), new Vector4(1f, 0f, 0f, 0f), new Vector4(0f, 1f, 0f, 0f), new Vector4(0f, 0f, 1f, 0f));
					tx = 0.5f * (1f + (Vector3.Dot(v2, x) - xDot) * invRadius);
					ty = 0.5f * (1f + (Vector3.Dot(v2, y) - yDot) * invRadius);
					alpha = 1f - (Vector3.Dot(v2, lightDir) - lDot) * invShadowLength;
					vertices[vidx++] = new GFXVertexFormat.PCTTBN(new Vector4(v2.X, v2.Y, v2.Z, 1f), new Vector4(1f, 1f, 1f, alpha), new Vector2(tx, ty), new Vector4(1f, 0f, 0f, 0f), new Vector4(0f, 1f, 0f, 0f), new Vector4(0f, 0f, 1f, 0f));
				}
			}
			if (vidx > 0)
			{
				this._vb.Instance.SetData<GFXVertexFormat.PCTTBN>(vertices, 0, vidx, SetDataOptions.None);
			}
			return vidx / 3;
		}

		private void _FindPolys(Box3F searchBox, out List<StaticGeometryComponent.Triangle> triangles, out List<Vector3> pointList, out List<Vector3> normList)
		{
			MarbleShadow._query.WorldBox = searchBox;
			MarbleShadow._query.ResultList = MarbleShadow._queryResult;
			MarbleShadow._query.ResultList.Clear();
			T3DSceneGraph.Instance.Container.FindObjects(MarbleShadow._query);
			foreach (ISceneObject so in MarbleShadow._queryResult)
			{
				StaticGeometryComponent stat = so as StaticGeometryComponent;
				if (stat != null)
				{
					stat.FindPolys(searchBox, out triangles, out pointList, out normList);
					return;
				}
			}
			triangles = null;
			pointList = null;
			normList = null;
		}
	}
}
