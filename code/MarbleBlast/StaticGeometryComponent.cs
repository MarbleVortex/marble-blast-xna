using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.GFX;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.T3D;
using GarageGames.Torque.TS;
using GarageGames.Torque.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public class StaticGeometryComponent : TorqueComponent, ISceneObject3D, ISceneContainerObject, ISceneObject
	{
		public class Triangle
		{
			public int idx0;

			public int idx1;

			public int idx2;

			public int normalIdx;

			public int searchId;
		}

		public class TriangleNode
		{
			public StaticGeometryComponent.Triangle tri;

			public StaticGeometryComponent.TriangleNode next;
		}

		private float PointEpsilon = 1E-05f;

		private float DotEpsilon = 1E-05f;

		private static int _searchId;

		private static List<int> _leaves = new List<int>();

		private static List<StaticGeometryComponent.Triangle> _triangles;

		private static bool _debugMode = false;

		private Box3F _prevQuery = new Box3F(1E+11f, 1E+11f, 1E+11f, -1E+11f, -1E+11f, -1E+11f);

		private KDTree<StaticGeometryComponent.TriangleNode> _tree = new KDTree<StaticGeometryComponent.TriangleNode>();

		private List<Vector3> _points = new List<Vector3>();

		private List<Vector3> _normals = new List<Vector3>();

		private List<StaticGeometryComponent.Triangle> _foundPolys = new List<StaticGeometryComponent.Triangle>();

		private Box3F _bounds = new Box3F(1E+11f, 1E+11f, 1E+11f, -1E+11f, -1E+11f, -1E+11f);

		private SceneContainerData _sceneContainerData = new SceneContainerData();

		public Box3F WorldBox
		{
			get
			{
				return this._bounds;
			}
		}

		public Matrix Transform
		{
			get
			{
				return Matrix.Identity;
			}
		}

		public bool Visible
		{
			get
			{
				return true;
			}
		}

		public float VisibilityLevel
		{
			get
			{
				return 1f;
			}
		}

		public TorqueObjectType ObjectType
		{
			get
			{
				return base.Owner.ObjectType;
			}
		}

		public OnCollisionDelegate OnCollision
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		public SceneContainerData SceneContainerData
		{
			get
			{
				return this._sceneContainerData;
			}
			set
			{
				this._sceneContainerData = value;
			}
		}

		public float FindIntersectionTime(Vector3 pos0, Vector3 pos1, float radius)
		{
			Box3F searchBox = default(Box3F);
			float bigRad = radius * 1.05f;
			Vector3 radial = new Vector3(bigRad, bigRad, bigRad);
			searchBox.Min = (searchBox.Max = pos0 - radial);
			searchBox.Extend(pos0 + radial);
			searchBox.Extend(pos1 - radial);
			searchBox.Extend(pos1 + radial);
			List<StaticGeometryComponent.Triangle> triList;
			List<Vector3> pointList;
			List<Vector3> normList;
			this.FindPolys(searchBox, out triList, out pointList, out normList);
			float tmin = 2f;
			foreach (StaticGeometryComponent.Triangle tri in triList)
			{
				Vector3 v0 = pointList[tri.idx0];
				Vector3 v = pointList[tri.idx1];
				Vector3 v2 = pointList[tri.idx2];
				Vector3 norm = normList[tri.normalIdx];
				float t;
				Vector3 closest;
				if (Collision.IntersectMovingSphereTriangle(pos0, pos1, radius, v0, v, v2, norm, out t, out closest) && t < tmin)
				{
					norm = pos0 + t * (pos1 - pos0) - closest;
					if (norm.LengthSquared() > 1E-05f)
					{
						norm.Normalize();
						float dir = Vector3.Dot(norm, pos1 - pos0);
						if (dir < -0.0001f)
						{
							tmin = t;
						}
					}
				}
			}
			return tmin;
		}

		public void AddContact(Vector3 pos, float radius, List<CollisionInfo> contacts)
		{
			Box3F searchBox = default(Box3F);
			float bigRad = radius * 1.1f;
			Vector3 radial = new Vector3(bigRad, bigRad, bigRad);
			searchBox.Min = (searchBox.Max = pos - radial);
			searchBox.Extend(pos + radial);
			List<StaticGeometryComponent.Triangle> triList;
			List<Vector3> pointList;
			List<Vector3> normList;
			this.FindPolys(searchBox, out triList, out pointList, out normList);
			foreach (StaticGeometryComponent.Triangle tri in triList)
			{
				Vector3 v0 = pointList[tri.idx0];
				Vector3 v = pointList[tri.idx1];
				Vector3 v2 = pointList[tri.idx2];
				Vector3 closest;
				if (Collision.ClosestPtPointTriangle(pos, radius, v0, v, v2, normList[tri.normalIdx], out closest) && (pos - closest).LengthSquared() < radius * radius)
				{
					Vector3 normal = pos - closest;
					if (Vector3.Dot(pos - closest, normList[tri.normalIdx]) > 0f)
					{
						normal.Normalize();
						CollisionInfo info = default(CollisionInfo);
						info.normal = normal;
						info.point = closest;
						info.penetration = radius - Vector3.Dot(pos - closest, info.normal);
						info.restitution = 1f;
						info.friction = 1f;
						contacts.Add(info);
					}
				}
				else if (StaticGeometryComponent._debugMode && Collision.ClosestPtPointTriangle(pos, radius, v0, v, v2, normList[tri.normalIdx], out closest))
				{
					Assert.Fatal((pos - closest).Length() >= radius, "should have found contact");
				}
			}
		}

		public void Render(SceneRenderState srs)
		{
		}

		public void FindPolys(Box3F box, out List<StaticGeometryComponent.Triangle> triList, out List<Vector3> pointList, out List<Vector3> normList)
		{
			if (!this._prevQuery.Contains(box.Max) || !this._prevQuery.Contains(box.Min))
			{
				StaticGeometryComponent._searchId++;
				this._foundPolys.Clear();
				Vector3 extent = 0.55f * (box.Max - box.Min);
				Vector3 center = 0.5f * (box.Max + box.Min);
				box.Min = (box.Max = center);
				box.Min -= extent;
				box.Max += extent;
				this._prevQuery = box;
				int count = 0;
				int found = 0;
				StaticGeometryComponent._leaves.Clear();
				this._tree.QueryBox(box.Min, box.Max, true, StaticGeometryComponent._leaves);
				using (List<int>.Enumerator enumerator = StaticGeometryComponent._leaves.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						int leaf = enumerator.Current;
						for (StaticGeometryComponent.TriangleNode cur = this._tree.GetNodeData(leaf); cur != null; cur = cur.next)
						{
							if (cur.tri.searchId != StaticGeometryComponent._searchId)
							{
								cur.tri.searchId = StaticGeometryComponent._searchId;
								count++;
								Box3F triBox = default(Box3F);
								triBox.Min = (triBox.Max = this._points[cur.tri.idx0]);
								triBox.Extend(this._points[cur.tri.idx1]);
								triBox.Extend(this._points[cur.tri.idx2]);
								if (triBox.Overlaps(ref box))
								{
									this._foundPolys.Add(cur.tri);
									found++;
								}
							}
						}
					}
					goto IL_208;
				}
			}
			box = this._prevQuery;
			IL_208:
			if (StaticGeometryComponent._debugMode)
			{
				List<StaticGeometryComponent.Triangle> _foundPolys2 = new List<StaticGeometryComponent.Triangle>();
				foreach (StaticGeometryComponent.Triangle tri in StaticGeometryComponent._triangles)
				{
					Box3F triBox2 = default(Box3F);
					triBox2.Min = (triBox2.Max = this._points[tri.idx0]);
					triBox2.Extend(this._points[tri.idx1]);
					triBox2.Extend(this._points[tri.idx2]);
					if (triBox2.Overlaps(ref box))
					{
						_foundPolys2.Add(tri);
					}
				}
				Assert.Fatal(this._foundPolys.Count == _foundPolys2.Count, "Wrong number of tris found");
				for (int i = 0; i < this._foundPolys.Count; i++)
				{
					bool found2 = false;
					for (int j = 0; j < _foundPolys2.Count; j++)
					{
						if (this._foundPolys[i].idx0 == _foundPolys2[j].idx0 && this._foundPolys[i].idx1 == _foundPolys2[j].idx1 && this._foundPolys[i].idx2 == _foundPolys2[j].idx2 && this._foundPolys[i].normalIdx == _foundPolys2[j].normalIdx)
						{
							found2 = true;
							break;
						}
					}
					Assert.Fatal(found2, "poly missing");
				}
			}
			triList = this._foundPolys;
			pointList = this._points;
			normList = this._normals;
		}

		protected override bool _OnRegister(TorqueObject owner)
		{
			if (!base._OnRegister(owner))
			{
				return false;
			}
			List<ValueInterface<ISceneObject>> renderables = new List<ValueInterface<ISceneObject>>();
			base.Owner.Components.GetInterfaceList<ValueInterface<ISceneObject>>("render", "always", renderables);
			this._buildStaticData(renderables);
			TorqueEventManager.ListenEvents<int>(T3DSceneGraph.Instance.FirstRenderEvent, new TorqueEventDelegate<int>(this._onFirstRender));
			TorqueEventManager.ListenEvents<bool>(GFXDevice.Instance.DeviceReset, new TorqueEventDelegate<bool>(this._onReRender));
			TorqueEventManager.ListenEvents<bool>(GFXDevice.Instance.DeviceCreated, new TorqueEventDelegate<bool>(this._onReRender));
			BaseSceneGraph.Instance.AddObject(this);
			return true;
		}

		private void _buildStaticData(List<ValueInterface<ISceneObject>> renderables)
		{
			if (renderables.Count != 0)
			{
				if (StaticGeometryComponent._triangles == null)
				{
					StaticGeometryComponent._triangles = new List<StaticGeometryComponent.Triangle>();
				}
				StaticGeometryComponent._triangles.Clear();
				foreach (ValueInterface<ISceneObject> so in renderables)
				{
					TSRenderComponent render = so.Value as TSRenderComponent;
					if (render != null)
					{
						this._getPointsAndTris(render.ShapeInstance, render.Transform, this._points, this._normals, StaticGeometryComponent._triangles);
					}
				}
				for (int i = 0; i < this._points.Count; i++)
				{
					this._bounds.Extend(this._points[i]);
				}
				List<Box3F> boxes = new List<Box3F>();
				for (int j = 0; j < StaticGeometryComponent._triangles.Count; j++)
				{
					StaticGeometryComponent.Triangle tri = StaticGeometryComponent._triangles[j];
					Box3F triBox = default(Box3F);
					triBox.Min = (triBox.Max = this._points[tri.idx0]);
					triBox.Extend(this._points[tri.idx1]);
					triBox.Extend(this._points[tri.idx2]);
					boxes.Add(triBox);
				}
				List<Vector3> points = new List<Vector3>();
				for (int k = 0; k < this._points.Count; k++)
				{
					points.Add(this._points[k]);
				}
				this._tree.BoxesPerBin = 8;
				this._tree.MaxDepth = 9;
				this._tree.BuildTree(boxes);
				this._dumpTriangles(StaticGeometryComponent._triangles);
				this._DumpStats();
				if (!StaticGeometryComponent._debugMode)
				{
					StaticGeometryComponent._triangles.Clear();
					StaticGeometryComponent._triangles.TrimExcess();
				}
			}
		}

		private void _dumpTriangles(List<StaticGeometryComponent.Triangle> triangles)
		{
			foreach (StaticGeometryComponent.Triangle tri in triangles)
			{
				Box3F box = default(Box3F);
				box.Min = this._points[tri.idx0];
				box.Max = box.Min;
				box.Extend(this._points[tri.idx1]);
				box.Extend(this._points[tri.idx2]);
				StaticGeometryComponent._leaves.Clear();
				this._tree.QueryBox(box.Min, box.Max, true, StaticGeometryComponent._leaves);
				foreach (int leaf in StaticGeometryComponent._leaves)
				{
					StaticGeometryComponent.TriangleNode cur = this._tree.GetNodeData(leaf);
					StaticGeometryComponent.TriangleNode triNode = new StaticGeometryComponent.TriangleNode();
					triNode.tri = tri;
					triNode.next = cur;
					this._tree.SetNodeData(leaf, triNode);
				}
			}
		}

		private void _testBinSizes(List<StaticGeometryComponent.Triangle> triangles)
		{
			List<Vector3> points = new List<Vector3>();
			for (int i = 0; i < this._points.Count; i++)
			{
				points.Add(this._points[i]);
				this._bounds.Extend(this._points[i]);
			}
			List<Box3F> boxes = new List<Box3F>();
			for (int j = 0; j < triangles.Count; j++)
			{
				StaticGeometryComponent.Triangle tri = triangles[j];
				Box3F triBox = default(Box3F);
				triBox.Min = (triBox.Max = this._points[tri.idx0]);
				triBox.Extend(this._points[tri.idx1]);
				triBox.Extend(this._points[tri.idx2]);
				boxes.Add(triBox);
			}
			this._tree.BoxesPerBin = 8;
			this._tree.PointsPerBin = 3;
			this._tree.MaxDepth = 8;
			this._tree.BuildTree(points);
			this._dumpTriangles(triangles);
			this._DumpStats();
			this._tree.BuildTree(boxes);
			this._dumpTriangles(triangles);
			this._DumpStats();
			this._tree.MaxDepth = 9;
			this._tree.BuildTree(points);
			this._dumpTriangles(triangles);
			this._DumpStats();
			this._tree.BuildTree(boxes);
			this._dumpTriangles(triangles);
			this._DumpStats();
			this._tree.MaxDepth = 10;
			this._tree.BuildTree(points);
			this._dumpTriangles(triangles);
			this._DumpStats();
			this._tree.BuildTree(boxes);
			this._dumpTriangles(triangles);
			this._DumpStats();
		}

		private void _getPointsAndTris(ShapeInstance shapeInstance, Matrix mat, List<Vector3> points, List<Vector3> normals, List<StaticGeometryComponent.Triangle> triangles)
		{
			Shape shape = shapeInstance.GetShape();
			for (int i = 0; i < shape.nodes.Length; i++)
			{
				Node node = shape.nodes[i];
				if (node._firstObject >= 0)
				{
					GarageGames.Torque.TS.Object obj = shape.objects[node._firstObject];
					if (obj._startMeshIndex >= 0)
					{
						Mesh mesh = shape.meshes[obj._startMeshIndex];
						Matrix toworld = Matrix.Multiply(shapeInstance.NodeTransforms[i], mat);
						for (int j = 0; j < mesh._primitives.Length; j++)
						{
							DrawPrimitive draw = mesh._primitives[j];
							if ((draw._materialIndex & 3221225472u) == 0u)
							{
								for (int k = 0; k < (int)draw._numElements; k += 3)
								{
									short i2 = mesh._indices[(int)draw._start + k];
									short i3 = mesh._indices[(int)draw._start + k + 1];
									short i4 = mesh._indices[(int)draw._start + k + 2];
									Vector3 v0 = Vector3.Transform(mesh._verts[(int)i2], toworld);
									Vector3 v = Vector3.Transform(mesh._verts[(int)i3], toworld);
									Vector3 v2 = Vector3.Transform(mesh._verts[(int)i4], toworld);
									if (i2 != i3 && i2 != i4 && i3 != i4)
									{
										int idx0 = this._addPoint(points, v0);
										int idx = this._addPoint(points, v);
										int idx2 = this._addPoint(points, v2);
										Vector3 normal = Vector3.Cross(v2 - v0, v - v0);
										int normIdx = this._addNormal(normals, normal);
										triangles.Add(new StaticGeometryComponent.Triangle
										{
											idx0 = idx0,
											idx1 = idx,
											idx2 = idx2,
											normalIdx = normIdx
										});
									}
								}
							}
							else
							{
								float sign = 1f;
								short i5 = mesh._indices[(int)draw._start];
								short i6 = mesh._indices[(int)(draw._start + 1)];
								Vector3 v3 = Vector3.Transform(mesh._verts[(int)i5], toworld);
								Vector3 v4 = Vector3.Transform(mesh._verts[(int)i6], toworld);
								for (int l = 2; l < (int)draw._numElements; l++)
								{
									short i7 = mesh._indices[(int)draw._start + l];
									Vector3 v5 = Vector3.Transform(mesh._verts[(int)i7], toworld);
									if (i5 != i6 && i5 != i7 && i6 != i7)
									{
										int idx3 = this._addPoint(points, v3);
										int idx4 = this._addPoint(points, v4);
										int idx5 = this._addPoint(points, v5);
										Vector3 normal2 = Vector3.Cross(v5 - v3, v4 - v3);
										normal2.Normalize();
										if (sign < 0f)
										{
											normal2 *= -1f;
											TorqueUtil.Swap<int>(ref idx3, ref idx4);
										}
										int normIdx2 = this._addNormal(normals, normal2);
										triangles.Add(new StaticGeometryComponent.Triangle
										{
											idx0 = idx3,
											idx1 = idx4,
											idx2 = idx5,
											normalIdx = normIdx2
										});
									}
									v3 = v4;
									v4 = v5;
									i5 = i6;
									i6 = i7;
									sign *= -1f;
								}
							}
						}
					}
				}
			}
		}

		private int _addPoint(List<Vector3> list, Vector3 pnt)
		{
			int start = Math.Max(0, list.Count - 500);
			for (int i = start; i < list.Count; i++)
			{
				Vector3 pnti = list[i];
				if (Math.Abs(pnti.X - pnt.X) < this.PointEpsilon && Math.Abs(pnti.Y - pnt.Y) < this.PointEpsilon && Math.Abs(pnti.Z - pnt.Z) < this.PointEpsilon)
				{
					return i;
				}
			}
			list.Add(pnt);
			return list.Count - 1;
		}

		private int _addNormal(List<Vector3> list, Vector3 normal)
		{
			int start = Math.Max(0, list.Count - 500);
			for (int i = start; i < list.Count; i++)
			{
				if (Vector3.Dot(list[i], normal) > 1f - this.DotEpsilon)
				{
					return i;
				}
			}
			list.Add(normal);
			return list.Count - 1;
		}

		private void _DumpStats()
		{
			Box3F all = new Box3F(-1E+11f, -1E+11f, -1E+11f, 1E+11f, 1E+11f, 1E+11f);
			StaticGeometryComponent._leaves.Clear();
			this._tree.QueryBox(all.Min, all.Max, true, StaticGeometryComponent._leaves);
			float avgPerBin = 0f;
			int avgCount = 0;
			int minInBin = 1000;
			int maxInBin = -1;
			foreach (int leaf in StaticGeometryComponent._leaves)
			{
				int count = 0;
				for (StaticGeometryComponent.TriangleNode cur = this._tree.GetNodeData(leaf); cur != null; cur = cur.next)
				{
					count++;
				}
				avgCount++;
				avgPerBin += (float)count;
				minInBin = Math.Min(minInBin, count);
				maxInBin = Math.Max(maxInBin, count);
			}
			if (avgCount != 0)
			{
				avgPerBin /= (float)avgCount;
			}
			Console.WriteLine("total tris: {3}, avg tris per bin: {0}, min tris per bin: {1}, max tris per bin: {2}", new object[]
			{
				avgPerBin,
				minInBin,
				maxInBin,
				StaticGeometryComponent._triangles.Count
			});
		}

		private void _onFirstRender(string eventName, int val)
		{
			List<ValueInterface<ISceneObject>> renderables = new List<ValueInterface<ISceneObject>>();
			base.Owner.Components.GetInterfaceList<ValueInterface<ISceneObject>>("render", "always", renderables);
			this._createVBIB(renderables);
		}

		private void _onReRender(string eventName, bool reset)
		{
			if (reset)
			{
				List<ValueInterface<ISceneObject>> renderables = new List<ValueInterface<ISceneObject>>();
				base.Owner.Components.GetInterfaceList<ValueInterface<ISceneObject>>("render", "always", renderables);
				this._createVBIB(renderables);
			}
		}

		private void _createVBIB(List<ValueInterface<ISceneObject>> renderables)
		{
			int numVerts = 0;
			int numIndices = 0;
			foreach (ValueInterface<ISceneObject> so in renderables)
			{
				TSRenderComponent render = so.Value as TSRenderComponent;
				if (render != null)
				{
					ShapeInstance arg_31_0 = render.ShapeInstance;
					Shape shape = render.Shape;
					for (int i = 0; i < shape.nodes.Length; i++)
					{
						Node node = shape.nodes[i];
						if (node._firstObject >= 0)
						{
							GarageGames.Torque.TS.Object obj = shape.objects[node._firstObject];
							if (obj._startMeshIndex >= 0)
							{
								Mesh mesh = shape.meshes[obj._startMeshIndex];
								mesh.TakeInventory(ref numVerts, ref numIndices, shape);
							}
						}
					}
				}
			}
			GraphicsDevice arg_D9_0 = GFXDevice.Instance.Device;
			GFXVertexFormat.PCTTBN[] verts = null;
			short[] indices = null;
			Resource<VertexBuffer> vb;
			if (numVerts != 0)
			{
				int sizeInBytes = numVerts * GFXVertexFormat.VertexSize;
				vb = ResourceManager.Instance.CreateVertexBuffer(ResourceProfiles.ManualStaticVBProfile, sizeInBytes);
				verts = new GFXVertexFormat.PCTTBN[numVerts];
			}
			Resource<IndexBuffer> ib;
			if (numIndices != 0)
			{
				int sizeInBytes2 = numIndices * 2;
				ib = ResourceManager.Instance.CreateIndexBuffer(ResourceProfiles.ManualStaticIBProfile, sizeInBytes2, IndexElementSize.SixteenBits);
				indices = new short[numIndices];
			}
			int countVert = 0;
			int countIndex = 0;
			foreach (ValueInterface<ISceneObject> so2 in renderables)
			{
				TSRenderComponent render2 = so2.Value as TSRenderComponent;
				if (render2 != null)
				{
					ShapeInstance shapeInstance = render2.ShapeInstance;
					Shape shape2 = render2.Shape;
					shape2._initD3D = true;
					for (int j = 0; j < shape2.nodes.Length; j++)
					{
						Node node2 = shape2.nodes[j];
						if (node2._firstObject >= 0)
						{
							GarageGames.Torque.TS.Object obj2 = shape2.objects[node2._firstObject];
							if (obj2._startMeshIndex >= 0)
							{
								Mesh mesh2 = shape2.meshes[obj2._startMeshIndex];
								Matrix toworld = Matrix.Multiply(shapeInstance.NodeTransforms[j], render2.Transform);
								int numMeshVerts = 0;
								int numMeshIndices = 0;
								mesh2.TakeInventory(ref numMeshVerts, ref numMeshIndices, shape2);
								mesh2.CopyVBIB(verts, indices, countVert, countIndex, toworld);
								Assert.Fatal(numMeshVerts != 0, "Doh!  We assume one material type per mesh");
								mesh2._worldSpace = true;
								mesh2._vertOffset = countVert;
								mesh2._indexOffset = countIndex;
								mesh2._vb = vb;
								mesh2._ib = ib;
								countVert += numMeshVerts;
								countIndex += numMeshIndices;
							}
						}
					}
				}
			}
			if (verts != null)
			{
				vb.Instance.SetData<GFXVertexFormat.PCTTBN>(verts, 0, numVerts, SetDataOptions.None);
			}
			if (indices != null)
			{
				ib.Instance.SetData<short>(indices, 0, numIndices, SetDataOptions.None);
			}
		}
	}
}
