using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.Sim;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace GarageGames.Torque.Examples.MarbleDemo
{
    public class MarbleCameraComponent : TorqueComponent, ISceneCamera, IAnimatedObject
    {
        private const float DefaultCameraPitch = 0.4f;

        private const float MIN_CAM_PITCH = -0.35f;

        private const float MAX_CAM_PITCH = 1.5f;

        private const float CameraRadius = 0.25f;

        private const float CameraDistance = 2.5f;

        private ValueInPlaceInterface<float> _cameraX;

        private ValueInPlaceInterface<float> _cameraY;

        private CollisionInterface _collisionInterface;

        private InputInterface _input;

        private ValueInterface<Matrix> _renderTransform;

        private ValueInterface<bool> _oob;

        private float _lastYaw;

        private Vector3 _OOBCamPos;

        private bool _cameraInit;

        private Vector3 _lastCamPos;

        private float _radius = 0.2f;

        private bool _instantCamera = true;

        private static List<CollisionInfo> _contacts = new List<CollisionInfo>();

        public Matrix Transform
        {
            get
            {
                return this._getCameraTransform();
            }
        }

        [TorqueCloneIgnore]
        public bool IsOOB
        {
            get
            {
                return this._oob != null && this._oob.Value;
            }
            set
            {
                if (this._oob != null)
                {
                    this._oob.Value = value;
                }
            }
        }

        public float Radius
        {
            get
            {
                return this._radius;
            }
            set
            {
                this._radius = value;
            }
        }

        public bool InstantCamera
        {
            get
            {
                return this._instantCamera;
            }
            set
            {
                this._instantCamera = value;
            }
        }

        public MarbleCameraComponent()
        {
            this._cameraX = new ValueInPlaceInterface<float>();
            this._cameraY = new ValueInPlaceInterface<float>();
            this._cameraY.Value = 0.4f;
        }

        private Vector2 lastMouse;
        public void UpdateAnimation(float dt)
        {
            Vector2 lookControl = this._pushToSquare(this._input.Look);

            float centerX = GraphicsDeviceManager.DefaultBackBufferWidth / 2.0f;
            float centerY = GraphicsDeviceManager.DefaultBackBufferHeight / 2.0f;

            Vector2 currentMouse = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            Vector2 difference = currentMouse - lastMouse;

            difference *= 0.005f;
            if (Math.Abs(difference.Y) > 0.00000000001f)
                InstantCamera = true;

            this._cameraX.Value += difference.X;
            this._cameraY.Value += difference.Y;

            Mouse.SetPosition((int)centerX, (int)centerY);
            lastMouse = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            float yaw = this._applyNonlinearScale2(this._rescaleDeadZone(lookControl.X, 0.25f));
            if (yaw * yaw < this._lastYaw * this._lastYaw)
            {
                this._lastYaw = yaw;
            }
            else
            {
                if (this._lastYaw * yaw < 0f && (this._lastYaw > 0f || yaw > 0f))
                {
                    this._lastYaw = 0f;
                }
                if (yaw * yaw > 0.0400000028f && this._lastYaw * this._lastYaw < 0.0400000028f)
                {
                    if (yaw < 0f)
                    {
                        this._lastYaw = -0.2f;
                    }
                    else
                    {
                        this._lastYaw = 0.2f;
                    }
                }
                float deltaYaw = yaw - this._lastYaw;
                float deltaMax = dt * 1.7f;
                if (deltaYaw * deltaYaw <= deltaMax * deltaMax)
                {
                    this._lastYaw = yaw;
                }
                else if (deltaYaw < 0f)
                {
                    this._lastYaw -= deltaMax;
                }
                else
                {
                    this._lastYaw += deltaMax;
                }
            }
            yaw = this._lastYaw * 0.7f * 2f * 3.14f * 0.03f;
            float camSpin = yaw;
            float movePitch = this._rescaleDeadZone(-lookControl.Y, 0.7f);
            float destPitch = (movePitch > 0f) ? (movePitch * 1.1f + 0.4f) : (-movePitch * -0.75f + 0.4f);
            float delta = destPitch - this._cameraY.Value;
            float absDelta = Math.Abs(delta);
            float camPitchSpeed = this._computePitchSpeedFromDelta(absDelta) * dt * 0.8f;
            if (delta > 0f)
            {
                if (camPitchSpeed > delta)
                {
                    camPitchSpeed = delta;
                }
            }
            else
            {
                if (camPitchSpeed > -delta)
                {
                    camPitchSpeed = -delta;
                }
                camPitchSpeed = -camPitchSpeed;
            }
            if (!InstantCamera)
                this._cameraY.Value += camPitchSpeed;
            this._cameraX.Value += camSpin;
            this._cameraY.Value = MathHelper.Clamp(this._cameraY.Value, -0.35f, 1.5f);
            if ((double)this._cameraX.Value > 6.2831853071795862)
            {
                this._cameraX.Value -= 6.28318548f;
                return;
            }
            if (this._cameraX.Value < 0f)
            {
                this._cameraX.Value += 6.28318548f;
            }
        }

		public override void CopyTo(TorqueComponent obj)
		{
			base.CopyTo(obj);
			MarbleCameraComponent obj2 = (MarbleCameraComponent)obj;
			obj2.Radius = this.Radius;
		}

		private Matrix _getCameraTransform()
		{
			if (this._oob.Value)
			{
				return this._getOOBCamera();
			}
			Matrix camMat = this._getLookMatrix();
			Vector3 camForwardDir = MatrixUtil.MatrixGetRow(1, ref camMat);
			Vector3 camUpDir = MatrixUtil.MatrixGetRow(2, ref camMat);
			Vector3 matPos = this._renderTransform.Value.Translation;
			Vector3 preStartPos = matPos;
			Vector3 startPos = matPos + camUpDir * (this._radius + 0.25f);
			Vector3 endPos = startPos - 2.5f * camForwardDir;
			Vector3 position = endPos;
			bool tryAgain = true;
			if ((this._lastCamPos - endPos).LengthSquared() > 100f)
			{
				this._cameraInit = false;
			}
			if (this._cameraInit && this._isCameraClear(this._lastCamPos, endPos))
			{
				position = endPos;
				tryAgain = false;
			}
			if (tryAgain)
			{
				if (this._isCameraClear(preStartPos, startPos))
				{
					preStartPos = startPos;
				}
				else
				{
					preStartPos = matPos;
				}
				this._moveCamera(preStartPos, endPos, out position, 4);
				if (this._isCameraClear(position, endPos))
				{
					position = endPos;
				}
			}
			endPos = position;
			this._lastCamPos = endPos;
			this._cameraInit = true;
			this._OOBCamPos = endPos;
			camForwardDir = startPos - endPos;
			camForwardDir.Normalize();
			Vector3 camSideDir = Vector3.Cross(camForwardDir, camUpDir);
			camSideDir.Normalize();
			camUpDir = Vector3.Cross(camSideDir, camForwardDir);
			camUpDir.Normalize();
			MatrixUtil.MatrixSetRow(0, ref camMat, ref camSideDir);
			MatrixUtil.MatrixSetRow(1, ref camMat, ref camForwardDir);
			MatrixUtil.MatrixSetRow(2, ref camMat, ref camUpDir);
			MatrixUtil.MatrixSetRow(3, ref camMat, ref endPos);
			return camMat;
		}

		private bool _isCameraClear(Vector3 start, Vector3 end)
		{
			float MaxCameraPenetration = 0.05f;
			Vector3 result;
			bool ret = this._moveCamera(start, end, out result, 1);
			if (ret)
			{
				(this._collisionInterface.Owner as MarbleCollisionComponent).IgnoreMarbles = true;
				MarbleCameraComponent._contacts.Clear();
				Quaternion norot = new Quaternion(0f, 0f, 0f, 1f);
				this._collisionInterface.FindContacts(end, norot, MarbleCameraComponent._contacts);
				float maxPenetration = 0f;
				foreach (CollisionInfo info in MarbleCameraComponent._contacts)
				{
					maxPenetration = Math.Max(maxPenetration, info.penetration);
				}
				(this._collisionInterface.Owner as MarbleCollisionComponent).IgnoreMarbles = false;
				return maxPenetration < MaxCameraPenetration;
			}
			return ret;
		}

		private bool _moveCamera(Vector3 start, Vector3 end, out Vector3 result, int maxIterations)
		{
			float CameraVelThreshold = 0.001f;
			(this._collisionInterface.Owner as MarbleCollisionComponent).IgnoreMarbles = true;
			Vector3 velocity = end - start;
			float remaining = 1f;
			int iterations = 0;
			float rebound = 1.05f;
			while (remaining > 0f && iterations++ < maxIterations && velocity.LengthSquared() > CameraVelThreshold * CameraVelThreshold)
			{
				Quaternion norot = new Quaternion(0f, 0f, 0f, 1f);
				remaining = this._collisionInterface.update(ref end, start, ref norot, norot, remaining, null);
				start = end;
				if (remaining > 1E-05f)
				{
					MarbleCameraComponent._contacts.Clear();
					this._collisionInterface.FindContacts(start, norot, MarbleCameraComponent._contacts);
					foreach (CollisionInfo info in MarbleCameraComponent._contacts)
					{
						float dot = Vector3.Dot(velocity, info.normal);
						if (dot < 0f)
						{
							velocity -= rebound * info.normal * Vector3.Dot(velocity, info.normal);
						}
					}
					rebound += 0.05f;
					end += remaining * velocity;
				}
			}
			(this._collisionInterface.Owner as MarbleCollisionComponent).IgnoreMarbles = false;
			result = start;
			return remaining < 1E-05f;
		}

		private Matrix _getOOBCamera()
		{
			Matrix camMat = this._getLookMatrix();
			Vector3 camUpDir = MatrixUtil.MatrixGetRow(2, ref camMat);
			Vector3 matPos = this._renderTransform.Value.Translation;
			matPos += camUpDir * (this._radius + 0.25f);
			Vector3 camForwardDir = matPos - this._OOBCamPos;
			camForwardDir.Normalize();
			Vector3 camSideDir = Vector3.Cross(camForwardDir, camUpDir);
			camUpDir = Vector3.Cross(camSideDir, camForwardDir);
			camSideDir.Normalize();
			camUpDir.Normalize();
			MatrixUtil.MatrixSetRow(0, ref camMat, ref camSideDir);
			MatrixUtil.MatrixSetRow(1, ref camMat, ref camForwardDir);
			MatrixUtil.MatrixSetRow(2, ref camMat, ref camUpDir);
			MatrixUtil.MatrixSetRow(3, ref camMat, ref this._OOBCamPos);
			return camMat;
		}

		private Matrix _getLookMatrix()
		{
			Matrix xRot = Matrix.CreateRotationX(-this._cameraY.Value);
			Matrix zRot = Matrix.CreateRotationZ(-this._cameraX.Value);
			return Matrix.Multiply(xRot, zRot);
		}

		private float _applyNonlinearScale2(float value)
		{
			return value * value * value;
		}

		private Vector2 _pushToSquare(Vector2 dir)
		{
			float absX = Math.Abs(dir.X);
			float absY = Math.Abs(dir.Y);
			float dirLen = dir.Length() * 1.25f;
			if (dirLen > 1f)
			{
				dirLen = 1f;
			}
			float scale = (absX > absY) ? absX : absY;
			if (scale < 0.01f)
			{
				scale = 0.01f;
			}
			dir *= dirLen / scale;
			return dir;
		}

		private float _computePitchSpeedFromDelta(float delta)
		{
			if (delta < 0.314f)
			{
				delta = 0.314f;
			}
			else if (delta > 1.57f)
			{
				delta = 1.57f;
			}
			return delta * 4f;
		}

		private float _rescaleDeadZone(float value, float deadZone)
		{
			if (value > deadZone)
			{
				return (value - deadZone) / (1f - deadZone);
			}
			if (value < -deadZone)
			{
				return (value + deadZone) / (1f - deadZone);
			}
			return 0f;
		}

		protected override bool _OnRegister(TorqueObject owner)
		{
			if (!base._OnRegister(owner))
			{
				return false;
			}
			this._input = (base.Owner.Components.GetInterface("input", string.Empty) as InputInterface);
			this._collisionInterface = (base.Owner.Components.GetInterface("collision", string.Empty) as CollisionInterface);
			this._renderTransform = (base.Owner.Components.GetInterface("transform", "render") as ValueInterface<Matrix>);
			this._oob = (base.Owner.Components.GetInterface("bool", "oob") as ValueInterface<bool>);
			ProcessList.Instance.AddAnimationCallback(owner, this);
			return true;
		}

		protected override void _OnUnregister()
		{
			base._OnUnregister();
		}

		protected override void _RegisterInterfaces(TorqueObject owner)
		{
			owner.RegisterCachedInterface("float", "cameraX", this, this._cameraX);
			owner.RegisterCachedInterface("float", "cameraY", this, this._cameraY);
			base._RegisterInterfaces(owner);
		}
	}
}
