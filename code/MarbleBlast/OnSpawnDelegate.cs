using GarageGames.Torque.Components;
using System;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public delegate void OnSpawnDelegate(MarbleControlComponent marble, MoveComponent move);
}
