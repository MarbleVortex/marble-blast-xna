using GarageGames.Torque.Core;
using GarageGames.Torque.LightingSystem;
using GarageGames.Torque.Materials;
using GarageGames.Torque.Materials.DXEffects;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.T3D;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public class MarbleEffect : BaseDXEffect, ITextureMaterial, ICubemapMaterial
	{
		protected string _currentTechnique;

		protected string _baseTexName;

		protected string _normalMapName;

		protected float _specularPower;

		protected Vector4 _specularColor;

		protected CubemapData _cubeData;

		protected Resource<Texture> _baseTex;

		protected Resource<Texture> _normalMap;

		protected bool _parametersLoaded;

		protected EffectParameter _objTransformParameter;

		protected EffectParameter _eyePosParameter;

		protected EffectParameter _lightPosParameter;

		protected EffectParameter _lightDirParameter;

		protected EffectParameter _lightDiffParameter;

		protected EffectParameter _lightAmbParameter;

		protected EffectParameter _cubeTransformParameter;

		protected EffectParameter _cubeEyeVecParameter;

		protected string _cubeMapName = string.Empty;

		protected EffectParameter _baseTexParameter;

		protected EffectParameter _normalMapParameter;

		protected EffectParameter _cubeMapParameter;

		protected EffectParameter _specularPowerParameter;

		protected EffectParameter _specularColorParameter;

		protected bool _riInserted;

		public string Technique
		{
			get
			{
				return this._currentTechnique;
			}
			set
			{
				this._currentTechnique = value;
			}
		}

		public string BaseTex
		{
			get
			{
				return this._baseTexName;
			}
			set
			{
				this._baseTexName = value;
				this._baseTex.Invalidate();
			}
		}

		public Resource<Texture> Texture
		{
			get
			{
				return this._baseTex;
			}
		}

		public string NormalMap
		{
			get
			{
				return this._normalMapName;
			}
			set
			{
				this._normalMapName = value;
				this._normalMap.Invalidate();
			}
		}

		public string CubemapName
		{
			get
			{
				return this._cubeMapName;
			}
			set
			{
				this._cubeMapName = value;
			}
		}

		public CubemapData CubemapData
		{
			get
			{
				return this._cubeData;
			}
			set
			{
				this._cubeData = value;
			}
		}

		public float SpecularPower
		{
			get
			{
				return this._specularPower;
			}
			set
			{
				this._specularPower = value;
			}
		}

		public Vector4 SpecularColor
		{
			get
			{
				return this._specularColor;
			}
			set
			{
				this._specularColor = value;
			}
		}

		public void ResetConfiguration()
		{
			base.Name = string.Empty;
			this.Technique = string.Empty;
			this.BaseTex = null;
			this.NormalMap = null;
			this.CubemapData = null;
			this.SpecularPower = 1f;
			this.SpecularColor = new Vector4(1f, 1f, 1f, 1f);
		}

		public override void Init(SceneRenderState srs)
		{
			if (this._baseTexName != null && this._baseTex.IsNull)
			{
				this._baseTex = srs.Gfx.TextureManager.LoadTexture(this._baseTexName);
			}
			if (this._normalMapName != null && this._normalMap.IsNull)
			{
				this._normalMap = srs.Gfx.TextureManager.LoadTexture(this._normalMapName);
			}
			if (this._cubeData != null && this._cubeData.Cubemap == null)
			{
				this._cubeData.createMap();
			}
			base.Init(srs);
		}

		public override void Dispose()
		{
			base.Dispose();
			this._cubeData.Cubemap.Cleanup();
			this._parametersLoaded = false;
			this._baseTex.Invalidate();
			this._normalMap.Invalidate();
			this._cubeData = null;
		}

		protected void _LoadEffectParameters()
		{
			if (this._objTransformParameter == null)
			{
				this._objTransformParameter = EffectManager.Instance.GetParameter(this._effect, "objTrans");
			}
			if (this._eyePosParameter == null)
			{
				this._eyePosParameter = EffectManager.Instance.GetParameter(this._effect, "eyePos");
			}
			if (this._lightPosParameter == null)
			{
				this._lightPosParameter = EffectManager.Instance.GetParameter(this._effect, "lightPos");
			}
			if (this._lightDirParameter == null)
			{
				this._lightDirParameter = EffectManager.Instance.GetParameter(this._effect, "lightDir");
			}
			if (this._lightDiffParameter == null)
			{
				this._lightDiffParameter = EffectManager.Instance.GetParameter(this._effect, "lightDiffuse");
			}
			if (this._lightAmbParameter == null)
			{
				this._lightAmbParameter = EffectManager.Instance.GetParameter(this._effect, "lightAmbient");
			}
			if (this._cubeTransformParameter == null)
			{
				this._cubeTransformParameter = EffectManager.Instance.GetParameter(this._effect, "cubeTrans");
			}
			if (this._cubeEyeVecParameter == null)
			{
				this._cubeEyeVecParameter = EffectManager.Instance.GetParameter(this._effect, "cubeEyeVec");
			}
			if (this._baseTexParameter == null)
			{
				this._baseTexParameter = EffectManager.Instance.GetParameter(this._effect, "baseTex");
			}
			if (this._normalMapParameter == null)
			{
				this._normalMapParameter = EffectManager.Instance.GetParameter(this._effect, "normalMap");
			}
			if (this._cubeMapParameter == null)
			{
				this._cubeMapParameter = EffectManager.Instance.GetParameter(this._effect, "cubeMap");
			}
			if (this._specularPowerParameter == null)
			{
				this._specularPowerParameter = EffectManager.Instance.GetParameter(this._effect, "specularPower");
			}
			if (this._specularColorParameter == null)
			{
				this._specularColorParameter = EffectManager.Instance.GetParameter(this._effect, "specularColor");
			}
		}

		protected override void _ClearEffectParameters()
		{
			base._ClearEffectParameters();
			this._parametersLoaded = false;
			this._objTransformParameter = null;
			this._eyePosParameter = null;
			this._lightPosParameter = null;
			this._lightDirParameter = null;
			this._lightDiffParameter = null;
			this._lightAmbParameter = null;
			this._cubeTransformParameter = null;
			this._cubeEyeVecParameter = null;
			this._baseTexParameter = null;
			this._normalMapParameter = null;
			this._cubeMapParameter = null;
			this._specularPowerParameter = null;
			this._specularColorParameter = null;
		}

		protected override void _SetupEffectValues(SceneRenderState srs)
		{
			base._SetupEffectValues(srs);
			if (!this._parametersLoaded)
			{
				this._LoadEffectParameters();
				this._parametersLoaded = true;
			}
			Matrix objToWorld = srs.World.Top;
			if (this._objTransformParameter != null)
			{
				this._objTransformParameter.SetValue(objToWorld);
			}
			Vector3 objectPos = objToWorld.Translation;
			Matrix worldToObj = Matrix.Invert(objToWorld);
			Vector3 worldEyePos = srs.CameraTransform.Translation;
			Vector3 eyePos = Vector3.Transform(worldEyePos, worldToObj);
			if (this._eyePosParameter != null)
			{
				this._eyePosParameter.SetValue(new Vector4(eyePos.X, eyePos.Y, eyePos.Z, 0f));
			}
			DirectionalLightInfo light = BaseSceneGraph.Instance.GetLightManager().GetSunLight() as DirectionalLightInfo;
			if (light != null)
			{
				Vector3 lightPos = light.Direction * 10000f;
				Vector3 lightDir = light.Direction;
				Vector3 dirColor = light.DiffuseColor;
				Vector3 ambColor = light.AmbientColor;
				lightPos = Vector3.Transform(lightPos, worldToObj);
				if (this._lightPosParameter != null)
				{
					this._lightPosParameter.SetValue(new Vector4(lightPos.X, lightPos.Y, lightPos.Z, 1f));
				}
				lightDir = Vector3.TransformNormal(lightDir, worldToObj);
				if (this._lightDirParameter != null)
				{
					this._lightDirParameter.SetValue(new Vector4(lightDir.X, lightDir.Y, lightDir.Z, 0f));
				}
				if (this._lightDiffParameter != null)
				{
					this._lightDiffParameter.SetValue(new Vector4(dirColor.X, dirColor.Y, dirColor.Z, 1f));
				}
				if (this._lightAmbParameter != null)
				{
					this._lightAmbParameter.SetValue(new Vector4(ambColor.X, ambColor.Y, ambColor.Z, 1f));
				}
			}
			Matrix cubeTrans = worldToObj;
			Vector3 zeroPos = new Vector3(0f, 0f, 0f);
			cubeTrans.Translation = zeroPos;
			if (this._cubeTransformParameter != null)
			{
				this._cubeTransformParameter.SetValue(cubeTrans);
			}
			Vector3 cubeEyeVec = worldEyePos - objectPos;
			if (this._cubeEyeVecParameter != null)
			{
				this._cubeEyeVecParameter.SetValue(new Vector4(cubeEyeVec.X, cubeEyeVec.Y, cubeEyeVec.Z, 1f));
			}
			if (this._baseTexParameter != null && !this._baseTex.IsNull)
			{
				this._baseTexParameter.SetValue(this._baseTex.Instance);
			}
			if (this._normalMapParameter != null && !this._normalMap.IsNull)
			{
				this._normalMapParameter.SetValue(this._normalMap.Instance);
			}
			if (this._cubeMapParameter != null && this._cubeData != null && this._cubeData.Cubemap != null)
			{
				this._cubeMapParameter.SetValue(this._cubeData.Cubemap.Texture);
			}
			if (this._specularPowerParameter != null)
			{
				this._specularPowerParameter.SetValue(this._specularPower);
			}
			if (this._specularColorParameter != null)
			{
				this._specularColorParameter.SetValue(this._specularColor);
			}
			Assert.Fatal(this._currentTechnique != null, "doh, no technique set!");
			this._effect.Instance.CurrentTechnique = this._effect.Instance.Techniques[this._currentTechnique];
		}
	}
}
