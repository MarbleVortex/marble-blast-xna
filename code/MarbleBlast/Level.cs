﻿using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.GameUtil;
using GarageGames.Torque.Materials;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.T3D;
using GarageGames.Torque.TS;
using GarageGames.Torque.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GarageGames.Torque.Examples.MarbleDemo
{
    public class LevelManager
    {
        public PlayerMarble PlayerMarble
        {
            get;
            private set;
        }

        private List<Marble> _otherMarbles;

        private T3DSceneGraph _sceneGraph;
        private T3DSceneCamera _camera;

        public List<Vector3> SpawnGroup
        {
            get;
            private set;
        }

        private TorqueSafePtr<TorqueSet> _gemGroups;
        private SceneLoader _sceneLoader;
        private int _lastGroupIdx;

        public LevelManager(SceneLoader sceneLoader)
        {
            this._sceneGraph = new T3DSceneGraph();
            Assert.Fatal(this._sceneGraph != null, "3D scene graph should have been created by engine component");

            this._sceneGraph.FarDistance = 1500f;
            this._sceneGraph.FOV = 1.04719758f;
            this._sceneGraph.Container.MaxBinsPerObject = 16;

            PreloadMaterialDelegate preload = new PreloadMaterialDelegate(RenderMaterialManager.PreloadMaterials);
            this._sceneGraph.PreloadMaterials = preload;

            this._sceneLoader = sceneLoader;
            this.SpawnGroup = new List<Vector3>();
            this._otherMarbles = new List<Marble>();
        }

        public void LoadLevel(string levelName)
        {
            string levelBase = "data\\levels\\" + levelName + "\\" + levelName + "_";
            string dirName = new FileInfo(levelBase).Directory.FullName;
            if (!Directory.Exists(dirName))
            {
                throw new Exception("Level directory does not exist:" + dirName);
            }
            this._sceneLoader.Load(dirName + "\\levelData.xml");

            this._loadLevelContent(levelBase);
            this._initObjects(levelBase);
            this.MakeGemGroupVisible();
        }

        private Vector3 _loadLevelContent(string levelbase)
        {
            int platNum = 0;
            TorqueObject level = new TorqueObject();
            while (true)
            {
                string tryName = (platNum < 10) ? string.Concat(new object[]
                {
                    levelbase,
                    "0",
                    platNum,
                    ".dts"
                }) : (levelbase + platNum + ".dts");
                FileInfo fi = new FileInfo(tryName);
                if (!fi.Exists)
                {
                    break;
                }
                TSPlatformComponent plat = new TSPlatformComponent();
                plat.ShapeName = tryName;
                if (!this._loadGemGroup(plat))
                {
                    plat.RenderSelf = true;
                    level.Components.AddComponent(plat);
                }
                platNum++;
            }
            MoveComponent nomove = new MoveComponent();
            nomove.ExportRenderTransform = true;
            nomove.Static = true;
            Matrix levelMat = Matrix.Identity;
            nomove.Transform = levelMat;
            level.Components.AddComponent(nomove);
            StaticGeometryComponent geom = new StaticGeometryComponent();
            level.Components.AddComponent(geom);
            TorqueObjectDatabase.Instance.Register(level);
            Box3F bounds = new Box3F(1E+11f, 1E+11f, 1E+11f, -1E+11f, -1E+11f, -1E+11f);
            for (int i = 0; i < level.Components.GetNumComponents(); i++)
            {
                TSPlatformComponent c = level.Components.GetComponentByIndex(i) as TSPlatformComponent;
                if (c != null)
                {
                    bounds.Extend(c.WorldBox.Min);
                    bounds.Extend(c.WorldBox.Max);
                }
            }
            Vector3 spawn = 0.5f * (bounds.Min + bounds.Max);
            if (!this._loadSpawns(levelbase, ref spawn))
            {
                spawn.Z = bounds.Max.Z + 4f;
            }
            return spawn;
        }

        private void _initObjects(string levelBase)
        {
            TorqueObject astrolabe = new TorqueObject();
            AstrolabeComponent astroTS = new AstrolabeComponent();
            astroTS.ShapeName = "data\\shapes\\astrolabe\\astrolabe.dts";
            astroTS.LoadSequence("data\\shapes\\astrolabe\\astrolabe_root.dsq");
            astroTS.AddThread("action", "Root", true);
            MoveComponent astroMove = new MoveComponent();
            astroMove.ExportRenderTransform = true;
            astroMove.Transform = Matrix.CreateTranslation(new Vector3(0f, 0f, -500f));
            astroMove.Static = true;
            astrolabe.Components.AddComponent(astroTS);
            astrolabe.Components.AddComponent(astroMove);
            TorqueObjectDatabase.Instance.Register(astrolabe);

            Vector3 spawnPoint = this._loadLevelContent(levelBase);
            PlayerMarble = new PlayerMarble(spawnPoint, "data\\shapes\\marbles\\marble10.dts");
            PlayerMarble.Name = "PlayerMarble";
            PlayerMarble.SetControl(true);
            MarbleCameraComponent cameraComponent = PlayerMarble.Components.FindComponent<MarbleCameraComponent>();
            T3DSceneGraph.Instance.Camera = cameraComponent;
            
            this._camera = new T3DSceneCamera();

            if (this.SpawnGroup.Count == 0)
            {
                Vector3 pos = spawnPoint;
                pos.Z += 20f;
                this.SpawnGroup.Add(pos);
            }
            this._spawnOtherMarbles();
        }

        private void _spawnOtherMarbles()
        {
            string[] otherMarbles = new string[]
            {
                "marble19.dts",
                "marble20.dts"
            };
            int marbIdx = 0;
            for (int spawnIdx = 0; spawnIdx < this.SpawnGroup.Count; spawnIdx++)
            {
                this._spawnOtherMarble("data\\shapes\\marbles\\" + otherMarbles[marbIdx++], spawnIdx);
                if (marbIdx >= otherMarbles.Length)
                {
                    break;
                }
            }
        }

        public void MakeGemGroupVisible()
        {
            if (this._gemGroups.Object == null || this._gemGroups.Object.GetNumItems() == 0)
            {
                return;
            }
            int count = this._gemGroups.Object.GetNumItems();
            int groupIdx = this._lastGroupIdx;
            if (count > 1)
            {
                do
                {
                    groupIdx = TorqueUtil.GetRandomInt(count);
                }
                while (groupIdx == this._lastGroupIdx);
            }
            TorqueFolder gemGroup = this._gemGroups.Object.GetItem(groupIdx) as TorqueFolder;
            if (gemGroup != null)
            {
                for (int i = 0; i < gemGroup.GetNumObjects(); i++)
                {
                    TorqueObject gem = gemGroup.GetObject(i);
                    for (int j = 0; j < gem.Components.GetNumComponents(); j++)
                    {
                        TSRenderComponent c = gem.Components.GetComponentByIndex(j) as TSRenderComponent;
                        if (c != null)
                        {
                            c.Visible = true;
                            break;
                        }
                    }
                }
            }
            this._lastGroupIdx = groupIdx;
        }

        private bool _loadGemGroup(TSPlatformComponent platform)
        {
            bool foundGroup = false;
            Shape shape = platform.Shape;
            int numNames = shape.names.Length;
            Vector3 groupOffset = default(Vector3);
            for (int idx = 0; idx < numNames; idx++)
            {
                string name = shape.names[idx];
                if (string.Compare("gemgroup", 0, name, 0, "gemgroup".Length, true) == 0)
                {
                    foundGroup = true;
                    groupOffset = shape.defaultTranslations[shape.FindNode(idx)];
                    break;
                }
            }
            if (!foundGroup)
            {
                return false;
            }
            TorqueFolder gemGroup = new TorqueFolder();
            TorqueObjectDatabase.Instance.Register(gemGroup);
            for (int idx = 0; idx < numNames; idx++)
            {
                string name = shape.names[idx];
                bool foundYellow;
                bool foundRed;
                bool foundBlue = foundRed = (foundYellow = false);
                if (string.Compare("red", 0, name, 0, "red".Length, true) == 0)
                {
                    foundRed = true;
                }
                else if (string.Compare("blue", 0, name, 0, "blue".Length, true) == 0)
                {
                    foundBlue = true;
                }
                else if (string.Compare("yellow", 0, name, 0, "yellow".Length, true) == 0)
                {
                    foundYellow = true;
                }
                if (foundRed || foundBlue || foundYellow)
                {
                    Vector3 gemPos = groupOffset + shape.defaultTranslations[shape.FindNode(idx)];

                    string skin = "yellow";
                    if (foundRed)
                    {
                        skin = "red";
                    }
                    else if (foundBlue)
                    {
                        skin = "blue";
                    }
                    Gem gem = new Gem(gemPos, skin);
                    gem.Folder = gemGroup;
                }
            }
            if (this._gemGroups.Object == null)
            {
                this._gemGroups.Object = new TorqueSet();
                TorqueObjectDatabase.Instance.Register(this._gemGroups);
            }
            this._gemGroups.Object.AddItem(gemGroup);
            return true;
        }

        private bool _loadSpawns(string levelbase, ref Vector3 startPos)
        {
            bool foundStartPad = false;
            string file = levelbase + "StartPad.dts";
            FileInfo fi = new FileInfo(file);
            if (fi.Exists)
            {
                Shape shape = new TSPlatformComponent
                {
                    ShapeName = file
                }.Shape;
                int numNames = shape.names.Length;
                for (int idx = 0; idx < numNames; idx++)
                {
                    string name = shape.names[idx];
                    if (string.Compare("startpad", 0, name, 0, "startpad".Length, true) == 0)
                    {
                        foundStartPad = true;
                        startPos = shape.defaultTranslations[shape.FindNode(idx)];
                        break;
                    }
                }
            }
            file = levelbase + "SpawnGroup.dts";
            fi = new FileInfo(file);
            if (fi.Exists)
            {
                Shape shape2 = new TSPlatformComponent
                {
                    ShapeName = file
                }.Shape;
                int numNames2 = shape2.names.Length;
                Vector3 groupOffset = default(Vector3);
                for (int idx2 = 0; idx2 < numNames2; idx2++)
                {
                    string name2 = shape2.names[idx2];
                    if (string.Compare("spawngroup", 0, name2, 0, "spawngroup".Length, true) == 0)
                    {
                        groupOffset = shape2.defaultTranslations[shape2.FindNode(idx2)];
                    }
                    else if (string.Compare("spawn", 0, name2, 0, "spawn".Length, true) == 0)
                    {
                        Vector3 position = shape2.defaultTranslations[shape2.FindNode(idx2)];
                        this.SpawnGroup.Add(groupOffset + position);
                    }
                }
            }
            return foundStartPad;
        }

        private void _spawnOtherMarble(string dtsPath, int spawnIdx)
        {
            if (this.SpawnGroup.Count == 0)
            {
                return;
            }

            Vector3 startPos = this.SpawnGroup[spawnIdx];

            Marble marble = new Marble(startPos, dtsPath);
            _otherMarbles.Add(marble);
        }

        public void ToggleCamera()
        {
            if (T3DSceneGraph.Instance.Camera == this._camera)
            {
                T3DSceneGraph.Instance.Camera = this.PlayerMarble.MarbleCamera;
                return;
            }
            T3DSceneGraph.Instance.Camera = this._camera;
        }
    }
}
