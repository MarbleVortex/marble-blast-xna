﻿using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.T3D;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GarageGames.Torque.Examples.MarbleDemo
{
    public class Gem : TorqueObject
    {
        public Gem(Vector3 position, string skin = "red")
        {
            Matrix gemMat = Matrix.Identity;
            gemMat.Translation = position;
            TSRenderComponent gemRender = new TSRenderComponent();
            gemRender.ShapeName = "data\\shapes\\gems\\gem.dts";
            gemRender.AddThread("action", "root", true);
            gemRender.OnCollision = new OnCollisionDelegate(this._gemCollection);
            gemRender.RenderSelf = true;
            gemRender.Visible = false;
            MoveComponent gemMove = new MoveComponent();
            gemMove.Transform = gemMat;
            gemMove.ExportRenderTransform = true;
            gemMove.Static = true;
            this.Components.AddComponent(gemRender);
            this.Components.AddComponent(gemMove);
            gemRender.ShapeInstance.reSkin(skin);
            TorqueObjectDatabase.Instance.Register(this);
            TorqueObjectDatabase.Instance.SetObjectType(this, "gem", true);
        }

        private void _gemCollection(ISceneObject3D obj, object marble)
        {
            MarbleControlComponent.SoundBank.PlayCue(MarbleControlComponent.Sound[13]);
            TSRenderComponent render = obj as TSRenderComponent;
            Assert.Fatal(render != null, "doh");
            render.Visible = false;
            TorqueFolder gemGroup = this.Folder;
            bool spawnGroup = true;
            for (int i = 0; i < gemGroup.GetNumObjects(); i++)
            {
                TorqueObject gem = gemGroup.GetObject(i);
                int j = 0;
                while (j < gem.Components.GetNumComponents())
                {
                    TSRenderComponent c = gem.Components.GetComponentByIndex(j) as TSRenderComponent;
                    if (c != null)
                    {
                        if (c.Visible)
                        {
                            spawnGroup = false;
                            break;
                        }
                        break;
                    }
                    else
                    {
                        j++;
                    }
                }
                if (!spawnGroup)
                {
                    break;
                }
            }
            if (spawnGroup)
            {
                MarbleBlast.Instance.LevelManager.MakeGemGroupVisible();
            }
        }
    }
}
