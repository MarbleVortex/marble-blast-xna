﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GarageGames.Torque.Examples.MarbleDemo
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            MarbleBlast game = new MarbleBlast();
            game.Run();
        }
    }
}
