using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.Sim;
using GarageGames.Torque.T3D;
using GarageGames.Torque.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public class MarbleControlComponent : TorqueComponent, IAnimatedObject
	{
		private class MarbleActuatorInterface : ActuatorInterface
		{
			public override void Update(float dt, ref Vector3 pos, ref Quaternion rot, ref Vector3 vel, ref Vector3 omega, List<CollisionInfo> contacts)
			{
				Assert.Fatal(base.Owner is MarbleControlComponent, "MarbleActuatorInterface must be owned by MarbleControlComponent");
				(base.Owner as MarbleControlComponent)._updateMove(dt, ref pos, ref rot, ref vel, ref omega, contacts);
			}

			public override void Setup(Vector3 pos, Quaternion rot)
			{
				Assert.Fatal(base.Owner is MarbleControlComponent, "MarbleActuatorInterface must be owned by MarbleControlComponent");
				(base.Owner as MarbleControlComponent)._setup(pos);
			}

			public override void Teardown(Vector3 pos, Quaternion rot)
			{
				Assert.Fatal(base.Owner is MarbleControlComponent, "MarbleActuatorInterface must be owned by MarbleControlComponent");
				(base.Owner as MarbleControlComponent)._teardown(pos);
			}
		}

		[Flags]
		public enum Sounds
		{
			RollHard = 0,
			RollMega = 1,
			RollIce = 2,
			Slip = 3,
			Bounce1 = 4,
			Bounce2 = 5,
			Bounce3 = 6,
			Bounce4 = 7,
			MegaBounce1 = 8,
			MegaBounce2 = 9,
			MegaBounce3 = 10,
			MegaBounce4 = 11,
			Jump = 12,
			GemPickup = 13,
			MaxSounds = 14
		}

		public static InputMap OOBInputMap = new InputMap();

		private MarbleControlComponent.MarbleActuatorInterface _actuator;

		private ValueInPlaceInterface<bool> _oobInterface;

		private CollisionInterface _collisionInterface;

		private InputInterface _input;

		private ValueInterface<float> _cameraX;

		private ValueInterface<float> _cameraY;

		private ValueInterface<Vector3> _velocity;

		private ValueInterface<Matrix> _worldTransform;

		private float _radius = 0.2f;

		private float _maxRollVelocity = 15f;

		private float _angularAcceleration = 75f;

		private float _jumpImpulse = 7.5f;

		private float _kineticFriction = 0.7f;

		private float _staticFriction = 1.1f;

		private float _brakingAcceleration = 30f;

		private float _gravity = 20f;

		private float _airAccel = 5f;

		private float _maxDotSlide = 0.5f;

		private float _minBounceVel = 0.1f;

		private float _bounceKineticFriction = 0.2f;

		private float _bounceRestitution = 0.5f;

		private bool _bounceYet;

		private float _bounceSpeed;

		private Vector3 _bouncePos;

		private Vector3 _bounceNormal;

		private float _slipAmount;

		private float _contactTime;

		private float _totalTime;

		private Cue _RollHandle;

		private Vector3 _startPos;

		private float _oobTime;

		private float _rollVolume;

		private Matrix _spawnPoint = Matrix.Identity;

		private static WaveBank _waveBank = null;

		private static SoundBank _soundBank = null;

		private static string[] _sounds = new string[14];

		private static T3DSceneContainerQueryData _itemQuery = new T3DSceneContainerQueryData();

		private static List<ISceneContainerObject> _itemList = new List<ISceneContainerObject>();

		private static OnSpawnDelegate _onSpawn;

		private static ParticleEmitterData _bounceEmitterData;

		public static WaveBank WaveBank
		{
			get
			{
				return MarbleControlComponent._waveBank;
			}
			set
			{
				MarbleControlComponent._waveBank = value;
			}
		}

		public static SoundBank SoundBank
		{
			get
			{
				return MarbleControlComponent._soundBank;
			}
			set
			{
				MarbleControlComponent._soundBank = value;
			}
		}

		public static string[] Sound
		{
			get
			{
				return MarbleControlComponent._sounds;
			}
		}

		public static OnSpawnDelegate OnMarbleSpawn
		{
			get
			{
				return MarbleControlComponent._onSpawn;
			}
			set
			{
				MarbleControlComponent._onSpawn = value;
			}
		}

		public static ParticleEmitterData BounceEmitterData
		{
			get
			{
				return MarbleControlComponent._bounceEmitterData;
			}
			set
			{
				MarbleControlComponent._bounceEmitterData = value;
			}
		}

		public float Radius
		{
			get
			{
				return this._radius;
			}
			set
			{
				this._radius = value;
			}
		}

		public Vector3 SpawnPoint
		{
			get
			{
				return this._spawnPoint.Translation;
			}
			set
			{
				this._spawnPoint.Translation = value;
			}
		}

		public Matrix SpawnMatrix
		{
			get
			{
				return this._spawnPoint;
			}
		}

		public MarbleControlComponent()
		{
			this._actuator = new MarbleControlComponent.MarbleActuatorInterface();
			this._oobInterface = new ValueInPlaceInterface<bool>();
			MarbleControlComponent._sounds[0] = "roll_hard";
			MarbleControlComponent._sounds[1] = "roll_mega";
			MarbleControlComponent._sounds[2] = "roll_ice";
			MarbleControlComponent._sounds[3] = "sliding";
			MarbleControlComponent._sounds[4] = "bounce_hard1";
			MarbleControlComponent._sounds[5] = "bounce_hard2";
			MarbleControlComponent._sounds[6] = "bounce_hard3";
			MarbleControlComponent._sounds[7] = "bounce_hard4";
			MarbleControlComponent._sounds[8] = "bounce_mega1";
			MarbleControlComponent._sounds[9] = "bounce_mega2";
			MarbleControlComponent._sounds[10] = "bounce_mega3";
			MarbleControlComponent._sounds[11] = "bounce_mega4";
			MarbleControlComponent._sounds[12] = "jump";
			MarbleControlComponent._sounds[13] = "gem_collect";
		}

		public void UpdateAnimation(float dt)
		{
			if (!TSPlatformComponent.SceneBounds.Contains(this._worldTransform.Value.Translation))
			{
				if (!this._oobInterface.Value)
				{
					if (this._input != null)
					{
						InputManager.Instance.PushInputMap(MarbleControlComponent.OOBInputMap);
					}
					this._oobTime = 0f;
					this._oobInterface.Value = true;
					return;
				}
				this._oobTime += dt;
				if (this._oobTime > 2f)
				{
					MoveComponent move = base.Owner.Components.FindComponent<MoveComponent>();
					if (MarbleControlComponent._onSpawn != null)
					{
						MarbleControlComponent._onSpawn(this, move);
					}
					if (this._input != null)
					{
						InputManager.Instance.PopInputMap(MarbleControlComponent.OOBInputMap);
					}
					this._oobInterface.Value = false;
					return;
				}
			}
			else
			{
				this._oobInterface.Value = false;
			}
		}

		public override void CopyTo(TorqueComponent obj)
		{
			base.CopyTo(obj);
			MarbleControlComponent obj2 = (MarbleControlComponent)obj;
			obj2.Radius = this.Radius;
			obj2.SpawnPoint = this.SpawnPoint;
		}

		protected override bool _OnRegister(TorqueObject owner)
		{
			base._OnRegister(owner);
			this._input = (base.Owner.Components.GetInterface("input", string.Empty) as InputInterface);
			this._collisionInterface = (base.Owner.Components.GetInterface("collision", string.Empty) as CollisionInterface);
			this._cameraX = (base.Owner.Components.GetInterface("float", "cameraX") as ValueInterface<float>);
			this._cameraY = (base.Owner.Components.GetInterface("float", "cameraY") as ValueInterface<float>);
			this._velocity = (base.Owner.Components.GetInterface("vector3", "velocity") as ValueInterface<Vector3>);
			this._worldTransform = (base.Owner.Components.GetInterface("transform", "render") as ValueInterface<Matrix>);
			ProcessList.Instance.AddAnimationCallback(owner, this);
			ParticleEmitterData arg_F7_0 = MarbleControlComponent._bounceEmitterData;
			return true;
		}

		protected override void _OnUnregister()
		{
			base._OnUnregister();
			this._RollHandle.Stop(AudioStopOptions.Immediate);
		}

		protected override void _RegisterInterfaces(TorqueObject owner)
		{
			owner.RegisterCachedInterface("actuator", string.Empty, this, this._actuator);
			owner.RegisterCachedInterface("bool", "oob", this, this._oobInterface);
			base._RegisterInterfaces(owner);
		}

		private void _setup(Vector3 pos)
		{
			this._bounceYet = false;
			this._slipAmount = 0f;
			this._contactTime = 0f;
			this._totalTime = 0f;
			this._startPos = pos;
		}

		private void _teardown(Vector3 pos)
		{
			this._updateBounceSound(pos);
			this._updateRollSound(pos);
			this._updateItems(this._startPos, pos);
		}

		private void _updateBounceSound(Vector3 pos)
		{
			if (this._bounceYet)
			{
				float minSize = 1f;
				float maxSize = 2f;
				float curSize = 1f;
				bool isMega = (curSize - minSize) / (maxSize - minSize) > 0.5f;
				float softBounceSpeed = isMega ? 2.5f : 2.5f;
				if (this._bounceSpeed >= softBounceSpeed)
				{
					float hardBounceSpeed = isMega ? 12f : 8f;
					int index = TorqueUtil.GetRandomInt(4);
					Assert.Fatal(index >= 0 && index < 4, "Bounce sound index out of range");
					index += (isMega ? 8 : 4);
					float minGain = isMega ? 0.2f : 0.5f;
					this._bounceSpeed *= 2f;
					float gain;
					if (this._bounceSpeed < hardBounceSpeed)
					{
						gain = (1f - minGain) * (this._bounceSpeed - softBounceSpeed) / (hardBounceSpeed - softBounceSpeed) + minGain;
					}
					else
					{
						gain = 1f;
					}
					if (MarbleControlComponent._sounds[index] != string.Empty)
					{
						Cue sound = MarbleControlComponent._soundBank.GetCue(MarbleControlComponent._sounds[index]);
						if (sound != null)
						{
							sound.SetVariable("MarbleGain", gain);
							sound.Play();
						}
					}
				}
			}
		}

		private void _updateRollSound(Vector3 pos)
		{
			float contactPct = 0f;
			if (this._totalTime > 0.0001f)
			{
				contactPct = this._contactTime / this._totalTime;
			}
			if (this._RollHandle == null)
			{
				this._RollHandle = MarbleControlComponent._soundBank.GetCue(MarbleControlComponent._sounds[0]);
				this._RollHandle.Play();
			}
			if (this._RollHandle == null)
			{
				return;
			}
			float megaAmt = 0f;
			float regAmt = 1f - megaAmt;
			float velLen = this._velocity.Value.Length();
			velLen *= 2f;
			float scale = velLen / 15f;
			float rollVolume = scale * 2f;
			if (rollVolume > 1f)
			{
				rollVolume = 1f;
			}
			if (contactPct < 0.001f)
			{
				rollVolume = 0f;
			}
			float rollSmooth = 0.25f;
			float useRoll = Math.Max(this._rollVolume, rollVolume);
			this._rollVolume = useRoll;
			this._rollVolume *= rollSmooth;
			this._rollVolume += rollVolume * (1f - rollSmooth);
			rollVolume = useRoll;
			this._RollHandle.SetVariable("MarbleGain", regAmt * rollVolume);
		}

		private void _updateItems(Vector3 startPos, Vector3 endPos)
		{
			Box3F box;
			box.Max = startPos;
			box.Min = startPos;
			box.Extend(endPos);
			Vector3 expansion = new Vector3(this._radius + 0.2f, this._radius + 0.2f, this._radius + 0.2f);
			box.Min -= expansion;
			box.Max += expansion;
			MarbleControlComponent._itemQuery.WorldBox = box;
			MarbleControlComponent._itemQuery.ResultList = MarbleControlComponent._itemList;
			MarbleControlComponent._itemList.Clear();
			T3DSceneGraph.Instance.Container.FindObjects(MarbleControlComponent._itemQuery);
			TorqueObjectType gemType = base.Owner.Manager.GetObjectType("gem");
			for (int i = 0; i < MarbleControlComponent._itemList.Count; i++)
			{
				TorqueComponent component = MarbleControlComponent._itemList[i] as TorqueComponent;
				if (component != null && component.Owner.TestObjectType(gemType))
				{
					ISceneObject3D sceneObj = component as ISceneObject3D;
					if (sceneObj != null && sceneObj.OnCollision != null)
					{
						Box3F gemBox = sceneObj.WorldBox;
						Vector3 gemCenter = 0.5f * (gemBox.Max + gemBox.Min);
						Vector3 gemExtent = 0.5f * (gemBox.Max - gemBox.Min);
						gemBox.Min = gemCenter - 0.5f * gemExtent;
						gemBox.Max = gemCenter + 0.5f * gemExtent;
						if (box.Overlaps(ref gemBox))
						{
							sceneObj.OnCollision(sceneObj, this);
						}
					}
				}
			}
		}

		private void _updateMove(float dt, ref Vector3 pos, ref Quaternion rot, ref Vector3 velocity, ref Vector3 omega, List<CollisionInfo> contacts)
		{
			this._collisionInterface.FindContacts(pos, rot, contacts);
			Vector3 aControl;
			Vector3 desiredOmega;
			bool isCentered = this._computeMoveForces(omega, out aControl, out desiredOmega);
			this._velocityCancel(contacts, ref velocity, ref omega, isCentered, false);
			Vector3 A = this._getExternalForces(dt, contacts);
			Vector3 a;
			this._applyContactForces(dt, contacts, isCentered, aControl, desiredOmega, ref velocity, ref omega, ref A, out a);
			velocity += A * dt;
			omega += a * dt;
			this._velocityCancel(contacts, ref velocity, ref omega, isCentered, true);
			this._totalTime += dt;
			if (contacts.Count != 0)
			{
				this._contactTime += dt;
			}
			contacts.Clear();
		}

		private bool _computeMoveForces(Vector3 omega, out Vector3 aControl, out Vector3 desiredOmega)
		{
			aControl = default(Vector3);
			desiredOmega = default(Vector3);
			Vector3 gWorkGravityDir = new Vector3(0f, 0f, -1f);
			Vector3 R = -gWorkGravityDir * this._radius;
			Vector3 rollVelocity = Vector3.Cross(omega, R);
			Vector3 sideDir;
			Vector3 motionDir;
			Vector3 upDir;
			this._getMarbleAxis(out sideDir, out motionDir, out upDir);
			float currentYVelocity = Vector3.Dot(rollVelocity, motionDir);
			float currentXVelocity = Vector3.Dot(rollVelocity, sideDir);
			Vector2 mv = (this._input != null) ? this._input.Move : default(Vector2);
			mv *= 1.53846157f;
			float mvlen = mv.Length();
			if (mvlen > 1f)
			{
				mv *= 1f / mvlen;
			}
			float desiredYVelocity = this._maxRollVelocity * mv.Y;
			float desiredXVelocity = this._maxRollVelocity * mv.X;
			if (desiredYVelocity != 0f || desiredXVelocity != 0f)
			{
				if (currentYVelocity > desiredYVelocity && desiredYVelocity > 0f)
				{
					desiredYVelocity = currentYVelocity;
				}
				else if (currentYVelocity < desiredYVelocity && desiredYVelocity < 0f)
				{
					desiredYVelocity = currentYVelocity;
				}
				if (currentXVelocity > desiredXVelocity && desiredXVelocity > 0f)
				{
					desiredXVelocity = currentXVelocity;
				}
				else if (currentXVelocity < desiredXVelocity && desiredXVelocity < 0f)
				{
					desiredXVelocity = currentXVelocity;
				}
				desiredOmega = Vector3.Cross(R, desiredYVelocity * motionDir + desiredXVelocity * sideDir) / R.LengthSquared();
				aControl = desiredOmega - omega;
				float aScalar = aControl.Length();
				if (aScalar > this._angularAcceleration)
				{
					aControl *= this._angularAcceleration / aScalar;
				}
				return false;
			}
			return true;
		}

		private void _applyContactForces(float dt, List<CollisionInfo> contacts, bool isCentered, Vector3 aControl, Vector3 desiredOmega, ref Vector3 velocity, ref Vector3 omega, ref Vector3 A, out Vector3 a)
		{
			a = default(Vector3);
			this._slipAmount = 0f;
			Vector3 gWorkGravityDir = new Vector3(0f, 0f, -1f);
			int bestSurface = -1;
			float bestNormalForce = 0f;
			for (int i = 0; i < contacts.Count; i++)
			{
				if (contacts[i].collider == null)
				{
					float normalForce = -Vector3.Dot(contacts[i].normal, A);
					if (normalForce > bestNormalForce)
					{
						bestNormalForce = normalForce;
						bestSurface = i;
					}
				}
			}
			CollisionInfo bestContact = (bestSurface != -1) ? contacts[bestSurface] : default(CollisionInfo);
			bool canJump = bestSurface != -1;
			if (canJump && this._input != null && this._input.LeftTrigger)
			{
				Vector3 velDifference = velocity - bestContact.velocity;
				float sv = Vector3.Dot(bestContact.normal, velDifference);
				if (sv < 0f)
				{
					sv = 0f;
				}
				if (sv < this._jumpImpulse)
				{
					velocity += bestContact.normal * (this._jumpImpulse - sv);
					MarbleControlComponent._soundBank.PlayCue(MarbleControlComponent._sounds[12]);
				}
			}
			for (int j = 0; j < contacts.Count; j++)
			{
				float normalForce2 = -Vector3.Dot(contacts[j].normal, A);
				if (normalForce2 > 0f && Vector3.Dot(contacts[j].normal, velocity - contacts[j].velocity) <= 0.0001f)
				{
					A += contacts[j].normal * normalForce2;
				}
			}
			if (bestSurface != -1)
			{
                // TODO: FIX
				//bestContact.velocity - bestContact.normal * Vector3.Dot(bestContact.normal, bestContact.velocity);
				Vector3 vAtC = velocity + Vector3.Cross(omega, -bestContact.normal * this._radius) - bestContact.velocity;
				float vAtCMag = vAtC.Length();
				bool slipping = false;
				Vector3 aFriction = new Vector3(0f, 0f, 0f);
				Vector3 AFriction = new Vector3(0f, 0f, 0f);
				if (vAtCMag != 0f)
				{
					slipping = true;
					float friction = this._kineticFriction * bestContact.friction;
					float angAMagnitude = 5f * friction * bestNormalForce / (2f * this._radius);
					float AMagnitude = bestNormalForce * friction;
					float totalDeltaV = (angAMagnitude * this._radius + AMagnitude) * dt;
					if (totalDeltaV > vAtCMag)
					{
						float fraction = vAtCMag / totalDeltaV;
						angAMagnitude *= fraction;
						AMagnitude *= fraction;
						slipping = false;
					}
					Vector3 vAtCDir = vAtC / vAtCMag;
					aFriction = angAMagnitude * Vector3.Cross(-bestContact.normal, -vAtCDir);
					AFriction = -AMagnitude * vAtCDir;
					this._slipAmount = vAtCMag - totalDeltaV;
				}
				if (!slipping)
				{
					Vector3 R = -gWorkGravityDir * this._radius;
					Vector3 aadd = Vector3.Cross(R, A) / R.LengthSquared();
					if (isCentered)
					{
						Vector3 nextOmega = omega + a * dt;
						aControl = desiredOmega - nextOmega;
						float aScalar = aControl.Length();
						if (aScalar > this._brakingAcceleration)
						{
							aControl *= this._brakingAcceleration / aScalar;
						}
					}
					Vector3 Aadd = -Vector3.Cross(aControl, -bestContact.normal * this._radius);
					float aAtCMag = (Vector3.Cross(aadd, -bestContact.normal * this._radius) + Aadd).Length();
					float friction2 = this._staticFriction * bestContact.friction;
					if (aAtCMag > friction2 * bestNormalForce)
					{
						friction2 = this._kineticFriction * bestContact.friction;
						Aadd *= friction2 * bestNormalForce / aAtCMag;
					}
					A += Aadd;
					a += aadd;
				}
				A += AFriction;
				a += aFriction;
			}
			a += aControl;
		}

		private Vector3 _getExternalForces(float dt, List<CollisionInfo> contacts)
		{
			Vector3 gWorkGravityDir = new Vector3(0f, 0f, -1f);
			Vector3 A = gWorkGravityDir * this._gravity;
			if (contacts.Count == 0 && this._input != null)
			{
				Vector3 sideDir;
				Vector3 motionDir;
				Vector3 upDir;
				this._getMarbleAxis(out sideDir, out motionDir, out upDir);
				A += (sideDir * this._input.Move.X + motionDir * this._input.Move.Y) * this._airAccel;
			}
			return A;
		}

		private void _velocityCancel(List<CollisionInfo> contacts, ref Vector3 velocity, ref Vector3 omega, bool surfaceSlide, bool noBounce)
		{
			new Vector3(0f, 0f, -1f);
			float SurfaceDotThreshold = 0.001f;
			bool looped = false;
			int itersIn = 0;
			bool done;
			do
			{
				done = true;
				itersIn++;
				for (int i = 0; i < contacts.Count; i++)
				{
					Vector3 sVel = velocity - contacts[i].velocity;
					float surfaceDot = Vector3.Dot(contacts[i].normal, sVel);
					if ((!looped && surfaceDot < 0f) || surfaceDot < -SurfaceDotThreshold)
					{
						float velLen = velocity.Length();
						Vector3 surfaceVel = surfaceDot * contacts[i].normal;
						this._reportBounce(contacts[i].point, contacts[i].normal, -surfaceDot);
						if (noBounce)
						{
							velocity -= surfaceVel;
						}
						else if (contacts[i].collider != null)
						{
							CollisionInfo info = contacts[i];
							MoveComponent otherMover = info.collider.Owner as MoveComponent;
							if (otherMover != null)
							{
								float ourMass = 1f;
								float theirMass = 1f;
								float bounce = 0.5f;
								Vector3 dp = velocity * ourMass - otherMover.Velocity * theirMass;
								Vector3 normP = Vector3.Dot(dp, info.normal) * info.normal;
								normP *= 1f + bounce;
								velocity -= normP / ourMass;
								otherMover.Velocity += normP / theirMass;
								info.velocity = otherMover.Velocity;
								contacts[i] = info;
							}
							else
							{
								float bounce2 = 0.5f;
								Vector3 normV = Vector3.Dot(velocity, info.normal) * info.normal;
								normV *= 1f + bounce2;
								velocity -= normV;
							}
						}
						else
						{
							Vector3 velocity2 = contacts[i].velocity;
							if (velocity2.Length() > 0.0001f && !surfaceSlide && surfaceDot > -this._maxDotSlide * velLen)
							{
								velocity -= surfaceVel;
								velocity.Normalize();
								velocity *= velLen;
								surfaceSlide = true;
							}
							else if (surfaceDot > -this._minBounceVel)
							{
								velocity -= surfaceVel;
							}
							else
							{
								float restitution = this._bounceRestitution;
								restitution *= contacts[i].restitution;
								Vector3 velocityAdd = -(1f + restitution) * surfaceVel;
								Vector3 vAtC = sVel + Vector3.Cross(omega, -contacts[i].normal * this._radius);
								float normalVel = -Vector3.Dot(contacts[i].normal, sVel);
								vAtC -= contacts[i].normal * Vector3.Dot(contacts[i].normal, sVel);
								float vAtCMag = vAtC.Length();
								if (vAtCMag != 0f)
								{
									float friction = this._bounceKineticFriction * contacts[i].friction;
									float angVMagnitude = 5f * friction * normalVel / (2f * this._radius);
									if (angVMagnitude > vAtCMag / this._radius)
									{
										angVMagnitude = vAtCMag / this._radius;
									}
									Vector3 vAtCDir = vAtC / vAtCMag;
									Vector3 deltaOmega = angVMagnitude * Vector3.Cross(-contacts[i].normal, -vAtCDir);
									omega += deltaOmega;
									velocity -= Vector3.Cross(-deltaOmega, -contacts[i].normal * this._radius);
								}
								velocity += velocityAdd;
							}
						}
						done = false;
					}
				}
				looped = true;
				if (itersIn > 6 && noBounce)
				{
					done = true;
				}
			}
			while (!done);
			if (velocity.LengthSquared() < 625f)
			{
				bool gotOne = false;
				Vector3 dir = new Vector3(0f, 0f, 0f);
				for (int j = 0; j < contacts.Count; j++)
				{
					Vector3 dir2 = dir + contacts[j].normal;
					if (dir2.LengthSquared() < 0.01f)
					{
						dir2 += contacts[j].normal;
					}
					dir = dir2;
					gotOne = true;
				}
				if (gotOne)
				{
					dir.Normalize();
					float soFar = 0f;
					for (int k = 0; k < contacts.Count; k++)
					{
						if (contacts[k].penetration < this._radius)
						{
							float timeToSeparate = 0.1f;
							float dist = contacts[k].penetration;
							float outVel = Vector3.Dot(velocity + soFar * dir, contacts[k].normal);
							if (timeToSeparate * outVel < dist)
							{
								soFar += (dist - outVel * timeToSeparate) / timeToSeparate / Vector3.Dot(contacts[k].normal, dir);
							}
						}
					}
					soFar = MathHelper.Clamp(soFar, -25f, 25f);
					velocity += soFar * dir;
				}
			}
		}

		private void _getMarbleAxis(out Vector3 sideDir, out Vector3 motionDir, out Vector3 upDir)
		{
			Vector3 gWorkGravityDir = new Vector3(0f, 0f, -1f);
			Matrix camMat = Matrix.Identity;
			if (this._cameraX != null && this._cameraY != null)
			{
				Matrix xRot = Matrix.CreateRotationX(-this._cameraY.Value);
				Matrix zRot = Matrix.CreateRotationZ(-this._cameraX.Value);
				camMat = Matrix.Multiply(xRot, zRot);
			}
			upDir = -gWorkGravityDir;
			motionDir = MatrixUtil.MatrixGetRow(1, ref camMat);
			sideDir = Vector3.Cross(motionDir, upDir);
			sideDir.Normalize();
			motionDir = Vector3.Cross(upDir, sideDir);
		}

		private void _reportBounce(Vector3 pos, Vector3 normal, float speed)
		{
			if (this._bounceYet && speed < this._bounceSpeed)
			{
				return;
			}
			this._bounceYet = true;
			this._bouncePos = pos;
			this._bounceSpeed = speed;
			this._bounceNormal = normal;
		}
	}
}
