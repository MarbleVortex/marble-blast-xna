using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.TS;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public class TSPlatformComponent : TSRenderComponent
	{
		private Box3F _shapeBox;

		private static List<Vector3> _verts = new List<Vector3>();

		private static Box3F _sceneBounds = new Box3F(1E+11f, 1E+11f, 1E+11f, -1E+11f, -1E+11f, -1E+11f);

		public static Box3F SceneBounds
		{
			get
			{
				return TSPlatformComponent._sceneBounds;
			}
			set
			{
				TSPlatformComponent._sceneBounds = value;
			}
		}

		public override Box3F WorldBox
		{
			get
			{
				return this._shapeBox;
			}
		}

		protected override bool _OnRegister(TorqueObject owner)
		{
			if (!base._OnRegister(owner))
			{
				return false;
			}
			this._findBox();
			Box3F box = TSPlatformComponent.SceneBounds;
			box.Extend(this._shapeBox.Min);
			box.Extend(this._shapeBox.Max);
			TSPlatformComponent.SceneBounds = box;
			BaseSceneGraph.Instance.UpdateObject(this);
			return true;
		}

		protected void _getMesh()
		{
			this._clearMesh();
			for (int i = 0; i < this._shape.nodes.Length; i++)
			{
				Node node = this._shape.nodes[i];
				if (node._firstObject >= 0)
				{
					GarageGames.Torque.TS.Object obj = this._shape.objects[node._firstObject];
					if (obj._startMeshIndex >= 0)
					{
						Mesh mesh = this._shape.meshes[obj._startMeshIndex];
						Matrix toworld = Matrix.Multiply(this._shapeInstance.NodeTransforms[i], this._worldTransformInterface.Value);
						for (int j = 0; j < mesh._verts.Length; j++)
						{
							this._addVert(Vector3.Transform(mesh._verts[j], toworld));
						}
					}
				}
			}
		}

		protected void _findBox()
		{
			this._getMesh();
			this._useBounds();
		}

		protected void _useBounds()
		{
			Vector3 minExtent = TSPlatformComponent._verts[0];
			Vector3 maxExtent = TSPlatformComponent._verts[0];
			for (int i = 1; i < TSPlatformComponent._verts.Count; i++)
			{
				Vector3 v = TSPlatformComponent._verts[i];
				if (v.X < minExtent.X)
				{
					minExtent.X = v.X;
				}
				else if (v.X > maxExtent.X)
				{
					maxExtent.X = v.X;
				}
				if (v.Y < minExtent.Y)
				{
					minExtent.Y = v.Y;
				}
				else if (v.Y > maxExtent.Y)
				{
					maxExtent.Y = v.Y;
				}
				if (v.Z < minExtent.Z)
				{
					minExtent.Z = v.Z;
				}
				else if (v.Z > maxExtent.Z)
				{
					maxExtent.Z = v.Z;
				}
			}
			this._shapeBox = new Box3F(minExtent, maxExtent);
		}

		private void _clearMesh()
		{
			TSPlatformComponent._verts.Clear();
		}

		private void _addVert(Vector3 vert)
		{
			TSPlatformComponent._verts.Add(vert);
		}

		private Vector3 _getVert(int idx)
		{
			return TSPlatformComponent._verts[idx];
		}
	}
}
