﻿using GarageGames.Torque.Sim;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Text;
using GarageGames.Torque.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GarageGames.Torque.Examples.MarbleDemo
{
    public class PlayerMarble : Marble
    {
        public PlayerMarble(Vector3 position, string dtsPath) : base(position, dtsPath)
        {
            int keyboardId = InputManager.Instance.FindDevice("keyboard");
            int gamepadId = InputManager.Instance.FindDevice("gamepad");
            MarbleControlComponent.OOBInputMap.BindCommand(keyboardId, (int)Keys.Space, new InputMap.MakeDelegate(this.RespawnOnOOB), null);
            MarbleControlComponent.OOBInputMap.BindCommand(gamepadId, 1024, new InputMap.MakeDelegate(this.RespawnOnOOB), null);
            MarbleControlComponent.OnMarbleSpawn = new OnSpawnDelegate(this.RespawnOnOOB);
        }

        protected override void RespawnOnOOB(MarbleControlComponent marble, MoveComponent move)
        {
            move.Transform = marble.SpawnMatrix;
            move.Velocity = default(Vector3);
            move.RotVelocity = default(Vector3);
        }
    }
}
