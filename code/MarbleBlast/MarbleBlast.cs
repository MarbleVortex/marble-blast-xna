using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.GameUtil;
using GarageGames.Torque.GUI;
using GarageGames.Torque.Materials;
using GarageGames.Torque.MathUtil;
using GarageGames.Torque.Platform;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.Sim;
using GarageGames.Torque.T3D;
using GarageGames.Torque.TS;
using GarageGames.Torque.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	public class MarbleBlast : TorqueGame
	{
        public static MarbleBlast Instance
        {
            get;
            private set;
        }

        public LevelManager LevelManager
        {
            get;
            private set;
        }

        public MarbleBlast()
        {
            MarbleBlast.Instance = this;
        }

		protected override void BeginRun()
		{
            // Since it can't be locked anyway.
            this.IsMouseVisible = false;

            this.LevelManager = new LevelManager(new SceneLoader());
            this._handleCommandLine();

            this.LevelManager.LoadLevel("DemoLevel");

            int keyboardId = InputManager.Instance.FindDevice("keyboard");
			InputMap.Global.BindCommand(keyboardId, (int)Keys.T, new InputMap.MakeDelegate(this.LevelManager.ToggleCamera), null);
            InputMap.Global.BindCommand(keyboardId, (int)Keys.Escape, new InputMap.MakeDelegate(this.Exit), null);
			InputUtil.BindStartBackQuickExit();

            

			GUIStyle playStyle = new GUIStyle();
			GUISceneview play = new GUISceneview();
			play.Name = "PlayScreen";
			play.Style = playStyle;
			GUICanvas.Instance.SetContentControl(play);
		}

        private void _handleCommandLine()
        {
            string[] args = Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                /*if (args[i] == "-level" && i + 1 < args.Length)
                {
                    levelBase = args[i + 1];
                }*/
                if (args[i] == "-particles")
                {
                    if (i + 1 < args.Length)
                    {
                        ParticleEmitter.TurnOn = bool.Parse(args[i + 1]);
                    }
                    else
                    {
                        ParticleEmitter.TurnOn = true;
                    }
                }
            }
        }
	}
}
