using GarageGames.Torque.Components;
using GarageGames.Torque.Core;
using GarageGames.Torque.SceneGraph;
using GarageGames.Torque.TS;
using System;

namespace GarageGames.Torque.Examples.MarbleDemo
{
	internal class AstrolabeComponent : TSRenderComponent
	{
		protected override bool _OnRegister(TorqueObject owner)
		{
			if (!base._OnRegister(owner))
			{
				return false;
			}
			Material[] matList = this._shapeInstance.GetShape().materialList;
			for (int i = 0; i < matList.Length; i++)
			{
				Material[] expr_27_cp_0 = matList;
				int expr_27_cp_1 = i;
				expr_27_cp_0[expr_27_cp_1].Flags = (expr_27_cp_0[expr_27_cp_1].Flags | MaterialFlags.Translucent);
			}
			return true;
		}

		public override void Render(SceneRenderState sceneRenderState)
		{
			base.Render(sceneRenderState);
		}
	}
}
